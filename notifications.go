package gotodonr

import (
	"net/http"
)

// Gets a page of notifications
//
//	excludeTypes: types to exclude (one or more of follow, favourite, reblog and/or mention)
//	   accountId: return notifications which were created due to actions performed by this account (optional)
//	       maxId: only retrieve statuses before this status (optional)
//	     sinceId: only retrieve statuses after this status (optional)
//	       minId: only retrieve statuses after this status (optional)
//	       limit: max amount of results per page (default: 20)
//
// Returns a slice of notifications and two urls pointing to the previous and next url respectively
func GetNotifications(baseUrl, accessToken string, excludeTypes []string, accountId, maxId, sinceId, minId string, limit int) ([]Notification, string, string, error) {
	data := map[string]interface{}{}
	if excludeTypes != nil {
		data["exclude_types"] = excludeTypes
	}
	if accountId != "" {
		data["account_id"] = accountId
	}
	if maxId != "" {
		data["max_id"] = maxId
	}
	if sinceId != "" {
		data["since_id"] = sinceId
	}
	if minId != "" {
		data["min_id"] = minId
	}
	if limit > 0 {
		data["limit"] = getLimit(limit)
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/notifications", data)
	var nots []Notification
	headers, _, err := apiCall(requestUrl, http.MethodGet, accessToken, nil, &nots)
	if err != nil {
		return nil, "", "", err
	}
	prev, next := readLinkHeader(headers)
	return nots, prev, next, nil
}

// Gets a book used to scroll through pages of notifications
//
//	excludeTypes: types to exclude (one or more of follow, favourite, reblog and/or mention)
//	   accountId: return notifications which were created due to actions performed by this account (optional)
//	       limit: max amount of results per page (default: 20)
//
// Returns a NotificationBook used to scroll through the pages or collect all at once
func GetNotificationsBook(baseUrl, accessToken string, excludeTypes []string, accountId string, limit int) NotificationBook {
	data := map[string]interface{}{}
	if excludeTypes != nil {
		data["exclude_types"] = excludeTypes
	}
	if accountId != "" {
		data["account_id"] = accountId
	}
	if limit > 0 {
		data["limit"] = getLimit(limit)
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/notifications", data)
	var book = NewNotificationBook(accessToken)
	book.Update(requestUrl)
	return book
}

// Get a notification
//
//	notificationId: identifier of the notification to get
//
// Returns the notifications
func GetNotification(baseUrl, accessToken, notificationId string) (Notification, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/notifications/"+notificationId)
	var not Notification
	_, _, err := apiCall(requestUrl, http.MethodGet, accessToken, nil, &not)
	if err != nil {
		return Notification{}, err
	}
	return not, nil
}

// Clear all notifications
func ClearNotifications(baseUrl, accessToken string) error {
	requestUrl := constructUrl(baseUrl, "/api/v1/notifications/clear")
	_, _, err := apiCall(requestUrl, http.MethodPost, accessToken, nil, nil)
	if err != nil {
		return err
	}
	return nil
}

// Dismisses a notification
//
//	notificationId: identifier of the notification to dismiss
//
// Returns the notifications
func DismissNotification(baseUrl, accessToken, notificationId string) error {
	requestUrl := constructUrl(baseUrl, "/api/v1/notifications/dismiss")
	_, _, err := apiCallJson(requestUrl, http.MethodPost, accessToken, map[string]interface{}{"id": notificationId}, nil)
	if err != nil {
		return err
	}
	return nil
}

// TODO: Subscriptions work so far (create, get, update, delete) but actually receiving them is untested

// Creates a new subscription
//
//	 endpoint: url of the push endpoint
//	   p256dh: public key (Base64 encoded string of public key of ECDH key using ‘prime256v1’ curve)
//	     auth: auth secret (Base64 encoded string of 16 bytes of random data)
//	   follow: receive follow notifications
//	favourite: receive favourite notifications
//	   reblog: receive reblog notifications
//	  mention: receive mention notifications
//	     poll: receive poll notifications
//
// Returns the newly created push subscription
func CreateSubscription(baseUrl, accessToken, endpoint, p256dh, auth string, follow, favourite, reblog, mention, poll bool) (PushSubscription, error) {
	data := map[string]interface{}{
		"subscription": map[string]interface{}{
			"endpoint": endpoint,
			"keys": map[string]interface{}{
				"p256dh": p256dh,
				"auth":   auth,
			},
		},
		"data": map[string]interface{}{
			"alerts": map[string]interface{}{
				"follow":    follow,
				"favourite": favourite,
				"reblog":    reblog,
				"mention":   mention,
				"poll":      poll,
			},
		},
	}
	requestUrl := constructUrl(baseUrl, "/api/v1/push/subscription")
	var sub PushSubscription
	_, _, err := apiCallJson(requestUrl, http.MethodPost, accessToken, data, &sub)
	if err != nil {
		return PushSubscription{}, err
	}
	return sub, nil
}

// Gets the current subscription
func GetSubscription(baseUrl, accessToken string) (PushSubscription, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/push/subscription")
	var sub PushSubscription
	_, _, err := apiCallJson(requestUrl, http.MethodGet, accessToken, nil, &sub)
	if err != nil {
		return PushSubscription{}, err
	}
	return sub, nil
}

// Updates the subscription
//
//	   follow: receive follow notifications
//	favourite: receive favourite notifications
//	   reblog: receive reblog notifications
//	  mention: receive mention notifications
//	     poll: receive poll notifications
//
// Returns the updated push subscription
func UpdateSubscription(baseUrl, accessToken string, follow, favourite, reblog, mention, poll bool) (PushSubscription, error) {
	data := map[string]interface{}{
		"data": map[string]interface{}{
			"alerts": map[string]interface{}{
				"follow":    follow,
				"favourite": favourite,
				"reblog":    reblog,
				"mention":   mention,
				"poll":      poll,
			},
		},
	}
	requestUrl := constructUrl(baseUrl, "/api/v1/push/subscription")
	var sub PushSubscription
	_, _, err := apiCallJson(requestUrl, http.MethodPut, accessToken, data, &sub)
	if err != nil {
		return PushSubscription{}, err
	}
	return sub, nil
}

// Deletes the current subscription
func DeleteSubscription(baseUrl, accessToken string) error {
	requestUrl := constructUrl(baseUrl, "/api/v1/push/subscription")
	_, _, err := apiCallJson(requestUrl, http.MethodDelete, accessToken, nil, nil)
	if err != nil {
		return err
	}
	return nil
}
