package gotodonr

import (
	"net/http"
)

// Get a servers custom emojis
//
// Returns a slice of emojis
func GetCustomEmojis(baseUrl string) ([]Emoji, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/custom_emojis")
	var emojis []Emoji
	_, _, err := apiCall(requestUrl, http.MethodGet, "", nil, &emojis)
	if err != nil {
		return nil, err
	}
	return emojis, nil
}
