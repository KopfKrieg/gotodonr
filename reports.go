package gotodonr

import "net/http"

// Reports a user
//
//	accountId: identifier of the account to report
//	statusIds: statuses which show violations of the ToS (optional)
//	  comment: additional information on why the user is being reported (optional, up to 1000 characters)
//	  forward: forward the report to the accounts server if it isn't the local server (default: false)
func Report(baseUrl, accessToken, accountId string, statusIds []string, comment string, forward bool) error {
	data := map[string]interface{}{
		"account_id": accountId,
		"forward":    forward,
	}
	if statusIds != nil {
		data["status_ids"] = statusIds
	}
	if comment != "" {
		data["comment"] = comment
	}
	requestUrl := constructUrl(baseUrl, "/api/v1/reports")
	_, _, err := apiCallJson(requestUrl, http.MethodPost, accessToken, data, nil)
	if err != nil {
		return err
	}
	return nil
}
