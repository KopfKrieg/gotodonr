package gotodonr

import "net/http"

// Search accounts, statuses and tags
//
//	query: query to look for
//	resolve: attempt a WebFinger lookup (default: false)
//	following: only include accounts the user is following (default: false)
//	limit: max number of results (default: 40)
// 	offset: result offset of the search (used for pagination) (default: 0)
//
// Returns a result, containing individual slices for accounts, statuses and tags
func Search(baseUrl, accessToken, query string, resolve, following bool, limit, offset int) (Result, error) {
	data := map[string]interface{}{
		"q":         query,
		"resolve":   resolve,
		"following": following,
		"limit":     getLimit(limit),
	}
	if offset > 0 {
		data["offset"] = offset
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v2/search", data)
	var res Result
	_, _, err := apiCall(requestUrl, http.MethodGet, accessToken, nil, &res)
	if err != nil {
		return Result{}, err
	}
	return res, nil
}
