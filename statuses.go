package gotodonr

import (
	"net/http"
)

// Gets a status
//
//	statusId: identifier of the status to get
//
// Returns the status
func GetStatus(baseUrl, statusId string) (Status, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/statuses/"+statusId)
	var stat Status
	_, _, err := apiCall(requestUrl, http.MethodGet, "", nil, &stat)
	if err != nil {
		return Status{}, err
	}
	return stat, nil
}

// Gets the context of a status
//
//	statusId: identifier of the status whose context to get
//
// Returns the context
func GetStatusContext(baseUrl, statusId string) (Context, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/statuses/"+statusId+"/context")
	var con Context
	_, _, err := apiCall(requestUrl, http.MethodGet, "", nil, &con)
	if err != nil {
		return Context{}, err
	}
	return con, nil
}

// Gets the card of a status
//
//	statusId: identifier of the status whose card to get
//
// Returns the card
func GetStatusCard(baseUrl, id string) (Card, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/statuses/"+id+"/card")
	var card Card
	_, _, err := apiCall(requestUrl, http.MethodGet, "", nil, &card)
	if err != nil {
		return Card{}, err
	}
	return card, nil
}

// Gets a page of accounts which reblogged a status
//
//	statusId: identifier of the status
//	   limit: max amount of results per page (default: 40)
//
// Returns a slice of accounts which reblogged the status and two urls pointing to the previous and next page respectively
func GetStatusRebloggedBy(baseUrl, statusId string, limit int) ([]Account, string, string, error) {
	data := map[string]interface{}{}
	if limit > 0 {
		data["limit"] = getLimit(limit)
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/statuses/"+statusId+"/reblogged_by", data)
	var accs []Account
	headers, _, err := apiCall(requestUrl, http.MethodGet, "", nil, &accs)
	if err != nil {
		return nil, "", "", err
	}
	prev, next := readLinkHeader(headers)
	return accs, next, prev, nil
}

// Gets a book used to scroll through the pages of accounts which reblogged a status
//
//	statusId: identifier of the status
//	   limit: max amount of results per page (default: 40)
//
// Returns an AccountBook which can be used to scroll through the pages or collect all at once
func GetStatusRebloggedByBook(baseUrl, statusId string, limit int) AccountBook {
	data := map[string]interface{}{}
	if limit > 0 {
		data["limit"] = getLimit(limit)
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/statuses/"+statusId+"/reblogged_by", data)
	var book = NewAccountBook("")
	book.Update(requestUrl)
	return book
}

// Gets a page of accounts which favourited a status
//
//	statusId: identifier of the status
//	   limit: max amount of results per page (default: 40)
//
// Returns a slice of accounts which favourited the status and two urls pointing to the previous and next page respectively
func GetStatusFavouritedBy(baseUrl, statusId string, limit int) ([]Account, string, string, error) {
	data := map[string]interface{}{}
	if limit > 0 {
		data["limit"] = getLimit(limit)
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/statuses/"+statusId+"/favourited_by", data)
	var accs []Account
	headers, _, err := apiCall(requestUrl, http.MethodGet, "", nil, &accs)
	if err != nil {
		return nil, "", "", err
	}
	prev, next := readLinkHeader(headers)
	return accs, next, prev, nil
}

// Gets a book used to scroll through the pages of accounts which favourited a status
//
//	statusId: identifier of the status
//	   limit: max amount of results per page (default: 40)
//
// Returns an AccountBook which can be used to scroll through the pages or collect all at once
func GetStatusFavouritedByBook(baseUrl, statusId string, limit int) AccountBook {
	data := map[string]interface{}{}
	if limit > 0 {
		data["limit"] = getLimit(limit)
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/statuses/"+statusId+"/favourited_by", data)
	var book = NewAccountBook("")
	book.Update(requestUrl)
	return book
}

// Posts a new status
//
//	        status: the text of the status
//	   inReplyToId: identifier of the status it will reply to (optional)
//	      mediaIds: identifiers of attachments (media status)
//	   pollOptions: options for the poll (poll status)
//	 pollExpiresIn: time in seconds the poll will be live (poll status) (default: DurationDay)
// 	  pollMultiple: set to true to allow multiple choices (poll status) (default: false)
// 	pollHideTotals: set to true to hide the results while the poll is live (poll status) (default: false)
// 	     sensitive: set to true if the post contains sensitive content (default: false)
//	   spoilerText: text which will be shown instead if the status is marked as sensitive (optional)
//	    visibility: visibility of the status (either public, private, unlisted or direct) (default: public)
//	      language: the language of the status (ISO 8601) (optional)
//	idempotencyKey: helps preventing duplicate posts (optional)
//
// Returns the newly posted status
func PostStatus(baseUrl, accessToken, status, inReplyToId string, mediaIds []string, pollOptions []string, pollExpiresIn int, pollMultiple, pollHideTotals, sensitive bool, spoilerText, visibility, language, idempotencyKey string) (Status, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/statuses")
	data := map[string]interface{}{
		"status":       status,
		"sensitive":    sensitive,
		"spoiler_text": spoilerText,
		"visibility":   visibility,
		"language":     language,
	}

	if inReplyToId != "" {
		data["in_reply_to_id"] = inReplyToId
	}

	if mediaIds != nil && len(mediaIds) > 0 {
		data["media_ids"] = mediaIds
	}

	if pollOptions != nil {
		pollData := map[string]interface{}{
			"options":     pollOptions,
			"expires_in":  pollExpiresIn,
			"multiple":    pollMultiple,
			"hide_totals": pollHideTotals,
		}
		data["poll"] = pollData
	}

	headers := map[string]string{}
	if idempotencyKey != "" {
		headers["Idempotency-Key"] = idempotencyKey
	}

	var stat Status
	_, _, err := apiCallJsonWithHeaders(requestUrl, http.MethodPost, accessToken, data, headers, &stat)
	if err != nil {
		return Status{}, err
	}
	return stat, nil
}

// Posts a new text status
//
//	        status: the text of the status
//	   inReplyToId: identifier of the status it will reply to (optional)
// 	     sensitive: set to true if the post contains sensitive content (default: false)
//	   spoilerText: text which will be shown instead if the status is marked as sensitive (optional)
//	    visibility: visibility of the status (either public, private, unlisted or direct) (default: public)
//	      language: the language of the status (ISO 8601) (optional)
//	idempotencyKey: helps preventing duplicate posts (optional)
//
// Returns the newly posted status
func PostTextStatus(baseUrl, accessToken, status, inReplyToId string, sensitive bool, spoilerText, visibility, language, idempotencyKey string) (Status, error) {
	return PostStatus(baseUrl, accessToken, status, inReplyToId, nil, nil, 0, false, false, sensitive, spoilerText, visibility, language, idempotencyKey)
}

// Posts a new media status
//
//	        status: the text of the status
//	   inReplyToId: identifier of the status it will reply to (optional)
//	      mediaIds: identifiers of attachments (media status)
// 	     sensitive: set to true if the post contains sensitive content (default: false)
//	   spoilerText: text which will be shown instead if the status is marked as sensitive (optional)
//	    visibility: visibility of the status (either public, private, unlisted or direct) (default: public)
//	      language: the language of the status (ISO 8601) (optional)
//	idempotencyKey: helps preventing duplicate posts (optional)
//
// Returns the newly posted status
func PostMediaStatus(baseUrl, accessToken, status, inReplyToId string, mediaIds []string, sensitive bool, spoilerText, visibility, language, idempotencyKey string) (Status, error) {
	return PostStatus(baseUrl, accessToken, status, inReplyToId, mediaIds, nil, 0, false, false, sensitive, spoilerText, visibility, language, idempotencyKey)
}

// Uploads any attachments and posts a new media status
//
//	        status: the text of the status
//	   inReplyToId: identifier of the status it will reply to (optional)
//	      mediaIds: identifiers of attachments (media status)
// 	     sensitive: set to true if the post contains sensitive content (default: false)
//	   spoilerText: text which will be shown instead if the status is marked as sensitive (optional)
//	    visibility: visibility of the status (either public, private, unlisted or direct) (default: public)
//	      language: the language of the status (ISO 8601) (optional)
//	idempotencyKey: helps preventing duplicate posts (optional)
//
// Returns the newly posted status
func UploadAndPostMediaStatus(baseUrl, accessToken, status, inReplyToId string, medias []Media, sensitive bool, spoilerText, visibility, language, idempotencyKey string) (Status, error) {
	var mediaIds []string
	for _, media := range medias {
		att, err := UploadMedia(baseUrl, accessToken, media.Path, media.Description, media.FocusX, media.FocusY)
		if err != nil {
			return Status{}, err
		}
		mediaIds = append(mediaIds, att.Id)
	}
	return PostMediaStatus(baseUrl, accessToken, status, inReplyToId, mediaIds, sensitive, spoilerText, visibility, language, idempotencyKey)
}

// Posts a new poll
//
//	        status: the text of the status
//	   inReplyToId: identifier of the status it will reply to (optional)
//	   pollOptions: options for the poll (poll status)
//	 pollExpiresIn: time in seconds the poll will be live (poll status) (default: DurationDay)
// 	  pollMultiple: set to true to allow multiple choices (poll status) (default: false)
// 	pollHideTotals: set to true to hide the results while the poll is live (poll status) (default: false)
// 	     sensitive: set to true if the post contains sensitive content (default: false)
//	   spoilerText: text which will be shown instead if the status is marked as sensitive (optional)
//	    visibility: visibility of the status (either public, private, unlisted or direct) (default: public)
//	      language: the language of the status (ISO 8601) (optional)
//	idempotencyKey: helps preventing duplicate posts (optional)
//
// Returns the newly posted status
func PostPollStatus(baseUrl, accessToken, status, inReplyToId string, pollOptions []string, pollExpiresIn int, pollMultiple, pollHideTotals, sensitive bool, spoilerText, visibility, language, idempotencyKey string) (Status, error) {
	return PostStatus(baseUrl, accessToken, status, inReplyToId, nil, pollOptions, pollExpiresIn, pollMultiple, pollHideTotals, sensitive, spoilerText, visibility, language, idempotencyKey)
}

// Deletes a status
//
//	statusId: identifier of the status to be deleted
func DeleteStatus(baseUrl, accessToken, statusId string) error {
	requestUrl := constructUrl(baseUrl, "/api/v1/statuses/"+statusId)
	_, _, err := apiCall(requestUrl, http.MethodDelete, accessToken, nil, nil)
	if err != nil {
		return err
	}
	return nil
}

// Reblogs a status
//
//	statusId: identifier of the status to be reblogged
func ReblogStatus(baseUrl, accessToken, statusId string) (Status, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/statuses/"+statusId+"/reblog")
	var sta Status
	_, _, err := apiCallJson(requestUrl, http.MethodPost, accessToken, nil, &sta)
	if err != nil {
		return Status{}, err
	}
	return sta, nil
}

// Unreblogs a status
//
//	statusId: identifier of the status to be unreblogged
func UnreblogStatus(baseUrl, accessToken, statusId string) (Status, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/statuses/"+statusId+"/unreblog")
	var sta Status
	_, _, err := apiCallJson(requestUrl, http.MethodPost, accessToken, nil, &sta)
	if err != nil {
		return Status{}, err
	}
	return sta, nil
}

// Pins a status
//
//	statusId: identifier of the status to be pinned
func PinStatus(baseUrl, accessToken, statusId string) (Status, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/statuses/"+statusId+"/pin")
	var sta Status
	_, _, err := apiCallJson(requestUrl, http.MethodPost, accessToken, nil, &sta)
	if err != nil {
		return Status{}, err
	}
	return sta, nil
}

// Unpins a status
//
//	statusId: identifier of the status to be unpinned
func UnpinStatus(baseUrl, accessToken, statusId string) (Status, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/statuses/"+statusId+"/unpin")
	var sta Status
	_, _, err := apiCallJson(requestUrl, http.MethodPost, accessToken, nil, &sta)
	if err != nil {
		return Status{}, err
	}
	return sta, nil
}
