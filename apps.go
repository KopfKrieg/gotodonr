package gotodonr

import (
	"net/http"
	"strings"
)

// Register your application as an OAuth application.
//
// 	 clientName: the name of the application (is shown on statuses)
//	    website: a website providing more information about your application (optional)
//	     scopes: the boundaries of scopes which can be requested
//	redirectUri: a list of possible redirect uris (optional)
//
// Returns an app with the application dataset (clientName, website, scope, redirectUris),
// clientId, clientSecret and vapidKey.
//
// Note: the scopes set when registering the application don't actually request anything. They only specify which
// scopes could potentially be requested during the apps lifecycle.
func RegisterApp(baseUrl, clientName, website, scope string, redirectUri ...string) (App, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/apps")
	if len(redirectUri) == 0 || (len(redirectUri) == 1 && redirectUri[0] == "") {
		redirectUri = []string{"urn:ietf:wg:oauth:2.0:oob"}
	}
	data := map[string]interface{}{
		"client_name":   clientName,
		"scopes":        scope,
		"website":       website,
		"redirect_uris": strings.Join(redirectUri, " "),
	}
	var app App
	_, _, err := apiCallJson(requestUrl, http.MethodPost, "", data, &app)
	if err != nil {
		return App{}, err
	}
	return app, nil
}

// Verifies that the AccessToken is at least a valid client token (contrary to an access token associated with an
// account). AccessTokens that are valid can be used to authorize the client but unless they were received
// using an authorization method, they can't be used to post statuses, update a profile or use other methods that
// require an account.
//
// See tootsuite/mastodon/app/controllers/api/v1/apps/credentials_controller.rb:
// https://github.com/tootsuite/mastodon/blob/master/app/controllers/api/v1/apps/credentials_controller.rb
func VerifyCredentials(baseUrl, accessToken string) (App, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/apps/verify_credentials")
	var app App
	_, _, err := apiCall(requestUrl, http.MethodGet, accessToken, nil, &app)
	if err != nil {
		return App{}, err
	}
	return app, nil
}

// Gets the account associated with the given accessToken.
// (This is a helper function which is not part of the API)
func GetAssociatedAccount(baseUrl, accessToken string) (Account, error) {
	return VerifyAccountCredentials(baseUrl, accessToken)
}
