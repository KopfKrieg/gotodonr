package gotodonr

import (
	"net/http"
)

// Gets a single page of statuses favourited by the user
//
//	    limit: maximum amount of accounts to retrieve (default: 40)
//
// Returns a slice of favourited statuses and two urls pointing to the next and previous page respectively
func GetFavourites(baseUrl, accessToken string, limit int) ([]Status, string, string, error) {
	data := map[string]interface{}{}
	if limit > 0 {
		data["limit"] = getLimit(limit)
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/favourites", data)
	var stats []Status
	headers, _, err := apiCall(requestUrl, http.MethodGet, accessToken, nil, &stats)
	if err != nil {
		return nil, "", "", err
	}
	prev, next := readLinkHeader(headers)
	return stats, next, prev, nil
}

// Gets a book which can be used to scroll through pages of statuses favourited by the user
//
//	    limit: maximum amount of accounts to retrieve per page (default: 40)
//
// Returns a StatusBook which can be used to scroll through the pages or collect all pages at once
func GetFavouritesBook(baseUrl, accessToken string, limit int) StatusBook {
	data := map[string]interface{}{}
	if limit > 0 {
		data["limit"] = getLimit(limit)
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/favourites", data)
	var book = NewStatusBook(accessToken)
	book.Update(requestUrl)
	return book
}

// Favourites a status
//
//	statusId: identifier of the status which should be favourited
//
// Returns the status
func FavouriteStatus(baseUrl, accessToken, statusId string) (Status, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/statuses/"+statusId+"/favourite")
	var sta Status
	_, _, err := apiCall(requestUrl, http.MethodPost, accessToken, nil, &sta)
	if err != nil {
		return Status{}, err
	}
	return sta, nil
}

// Unfavourites a status
//
//	statusId: identifier of the status which should be unfavourited
//
// Returns the status
func UnfavouriteStatus(baseUrl, accessToken, statusId string) (Status, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/statuses/"+statusId+"/unfavourite")
	var sta Status
	_, _, err := apiCall(requestUrl, http.MethodPost, accessToken, nil, &sta)
	if err != nil {
		return Status{}, err
	}
	return sta, nil
}
