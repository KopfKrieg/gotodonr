package gotodonr

import (
	"log"
	"net/http"
	"time"
)

// Assures that the limit is within bounds (greater than zero but smaller or equal to 80)
func getLimit(limit int) int {
	if limit <= 0 {
		limit = 40
	}
	if limit > 80 {
		limit = 80
	}
	return limit
}

// Helper struct for pagination
type Book struct {
	currentUrl  string // the url holding the current page of data
	prevUrl     string // the url holding the previous page of data
	nextUrl     string // the url holding the next page of data
	accessToken string // the access token used in requests (optional)
}

// Checks if the book has a link to the previous page
func (book *Book) HasPrevious() bool {
	return book.prevUrl != ""
}

// Checks if the book has a link to the next page
func (book *Book) HasNext() bool {
	return book.nextUrl != ""
}

//region Account Book

// Creates a new AccountBook
func NewAccountBook(accessToken string) AccountBook {
	var book = AccountBook{}
	book.accessToken = accessToken
	return book
}

// Book used to scroll through accounts
type AccountBook struct {
	Current []Account
	Book
}

// Updates the book to the values from the previous page
func (book *AccountBook) Previous() []Account {
	if book.HasPrevious() {
		return book.Update(book.prevUrl)
	} else {
		return book.Current
	}
}

// Updates the book to the values from the next page
func (book *AccountBook) Next() []Account {
	if book.HasNext() {
		return book.Update(book.nextUrl)
	} else {
		return book.Current
	}
}

// Returns all available accounts.
// Warning: Can take a long time (receives a maximum of 80 accounts per second)
func (book *AccountBook) All() []Account {
	var curr []Account
	curr = append(curr, book.Current...)
	var savepoint = book.currentUrl
	for book.HasPrevious() {
		accs := book.Previous()
		if accs != nil && len(accs) > 0 {
			curr = append(curr, accs...)
			time.Sleep(time.Second)
		} else {
			break
		}
	}
	if savepoint != "" {
		book.Update(savepoint)
		time.Sleep(time.Second)
		for book.HasNext() {
			accs := book.Next()
			if accs != nil && len(accs) > 0 {
				curr = append(curr, accs...)
				time.Sleep(time.Second)
			} else {
				break
			}
		}
	}
	book.Current = curr
	return book.Current
}

// Updates the book using the values retrieved from the given url
func (book *AccountBook) Update(url string) []Account {
	val, prev, next, err := book.Get(url, book.accessToken)
	if err != nil {
		log.Fatal(err)
	}
	book.Current = val
	book.currentUrl = url
	book.prevUrl = prev
	book.nextUrl = next
	return book.Current
}

// Retrieves a given page
func (book *AccountBook) Get(url, accessToken string) ([]Account, string, string, error) {
	var acc []Account
	headers, _, err := apiCall(url, http.MethodGet, accessToken, nil, &acc)
	if err != nil {
		return nil, "", "", err
	}
	prev, next := readLinkHeader(headers)
	return acc, prev, next, nil
}

//endregion
//region Status Book

// Creates a new StatusBook
func NewStatusBook(accessToken string) StatusBook {
	var book = StatusBook{}
	book.accessToken = accessToken
	return book
}

// Book used to scroll through statuses
type StatusBook struct {
	Current []Status
	Book
}

// Updates the book to the values from the previous page
func (book *StatusBook) Previous() []Status {
	if book.HasPrevious() {
		return book.Update(book.prevUrl)
	} else {
		return book.Current
	}
}

// Updates the book to the values from the next page
func (book *StatusBook) Next() []Status {
	if book.HasNext() {
		return book.Update(book.nextUrl)
	} else {
		return book.Current
	}
}

// Returns all available statuses.
// Warning: Can take a long time (receives a maximum of 80 accounts per second)
func (book *StatusBook) All() []Status {
	var curr []Status
	curr = append(curr, book.Current...)
	var savepoint = book.currentUrl
	for book.HasPrevious() {
		accs := book.Previous()
		if accs != nil && len(accs) > 0 {
			curr = append(curr, accs...)
			time.Sleep(time.Second)
		} else {
			break
		}
	}
	if savepoint != "" {
		book.Update(savepoint)
		time.Sleep(time.Second)
		for book.HasNext() {
			accs := book.Next()
			if accs != nil && len(accs) > 0 {
				curr = append(curr, accs...)
				time.Sleep(time.Second)
			} else {
				break
			}
		}
	}
	book.Current = curr
	return book.Current
}

// Updates the book using the values retrieved from the given url
func (book *StatusBook) Update(url string) []Status {
	val, prev, next, err := book.Get(url, book.accessToken)
	if err != nil {
		log.Fatal(err)
	}
	book.Current = val
	book.currentUrl = url
	book.prevUrl = prev
	book.nextUrl = next
	return book.Current
}

// Retrieves the page of data and link-urls from the given url
func (book *StatusBook) Get(url, accessToken string) ([]Status, string, string, error) {
	var acc []Status
	headers, _, err := apiCall(url, http.MethodGet, accessToken, nil, &acc)
	if err != nil {
		return nil, "", "", err
	}
	prev, next := readLinkHeader(headers)
	return acc, prev, next, nil
}

//endregion
//region String Book

// Creates a new StringBook
func NewStringBook(accessToken string) StringBook {
	var book = StringBook{}
	book.accessToken = accessToken
	return book
}

// Book used to scroll through statuses
type StringBook struct {
	Current []string
	Book
}

// Updates the book to the values from the previous page
func (book *StringBook) Previous() []string {
	if book.HasPrevious() {
		return book.Update(book.prevUrl)
	} else {
		return book.Current
	}
}

// Updates the book to the values from the next page
func (book *StringBook) Next() []string {
	if book.HasNext() {
		return book.Update(book.nextUrl)
	} else {
		return book.Current
	}
}

// Returns all available statuses.
// Warning: Can take a long time (receives a maximum of 80 accounts per second)
func (book *StringBook) All() []string {
	var curr []string
	curr = append(curr, book.Current...)
	var savepoint = book.currentUrl
	for book.HasPrevious() {
		accs := book.Previous()
		if accs != nil && len(accs) > 0 {
			curr = append(curr, accs...)
			time.Sleep(time.Second)
		} else {
			break
		}
	}
	if savepoint != "" {
		book.Update(savepoint)
		time.Sleep(time.Second)
		for book.HasNext() {
			accs := book.Next()
			if accs != nil && len(accs) > 0 {
				curr = append(curr, accs...)
				time.Sleep(time.Second)
			} else {
				break
			}
		}
	}
	book.Current = curr
	return book.Current
}

// Updates the book using the values retrieved from the given url
func (book *StringBook) Update(url string) []string {
	val, prev, next, err := book.Get(url, book.accessToken)
	if err != nil {
		log.Fatal(err)
	}
	book.Current = val
	book.currentUrl = url
	book.prevUrl = prev
	book.nextUrl = next
	return book.Current
}

// Retrieves the page of data and link-urls from the given url
func (book *StringBook) Get(url, accessToken string) ([]string, string, string, error) {
	var acc []string
	headers, _, err := apiCall(url, http.MethodGet, accessToken, nil, &acc)
	if err != nil {
		return nil, "", "", err
	}
	prev, next := readLinkHeader(headers)
	return acc, prev, next, nil
}

//endregion
//region Notification Book

// Creates a new NotificationBook
func NewNotificationBook(accessToken string) NotificationBook {
	var book = NotificationBook{}
	book.accessToken = accessToken
	return book
}

// Book used to scroll through statuses
type NotificationBook struct {
	Current []Notification
	Book
}

// Updates the book to the values from the previous page
func (book *NotificationBook) Previous() []Notification {
	if book.HasPrevious() {
		return book.Update(book.prevUrl)
	} else {
		return book.Current
	}
}

// Updates the book to the values from the next page
func (book *NotificationBook) Next() []Notification {
	if book.HasNext() {
		return book.Update(book.nextUrl)
	} else {
		return book.Current
	}
}

// Returns all available statuses.
// Warning: Can take a long time (receives a maximum of 80 accounts per second)
func (book *NotificationBook) All() []Notification {
	var curr []Notification
	curr = append(curr, book.Current...)
	var savepoint = book.currentUrl
	for book.HasPrevious() {
		accs := book.Previous()
		if accs != nil && len(accs) > 0 {
			curr = append(curr, accs...)
			time.Sleep(time.Second)
		} else {
			break
		}
	}
	if savepoint != "" {
		book.Update(savepoint)
		time.Sleep(time.Second)
		for book.HasNext() {
			accs := book.Next()
			if accs != nil && len(accs) > 0 {
				curr = append(curr, accs...)
				time.Sleep(time.Second)
			} else {
				break
			}
		}
	}
	book.Current = curr
	return book.Current
}

// Updates the book using the values retrieved from the given url
func (book *NotificationBook) Update(url string) []Notification {
	val, prev, next, err := book.Get(url, book.accessToken)
	if err != nil {
		log.Fatal(err)
	}
	book.Current = val
	book.currentUrl = url
	book.prevUrl = prev
	book.nextUrl = next
	return book.Current
}

// Retrieves the page of data and link-urls from the given url
func (book *NotificationBook) Get(url, accessToken string) ([]Notification, string, string, error) {
	var acc []Notification
	headers, _, err := apiCall(url, http.MethodGet, accessToken, nil, &acc)
	if err != nil {
		return nil, "", "", err
	}
	prev, next := readLinkHeader(headers)
	return acc, prev, next, nil
}

//endregion
//region Conversation Book

// Creates a new ConversationBook
func NewConversationBook(accessToken string) ConversationBook {
	var book = ConversationBook{}
	book.accessToken = accessToken
	return book
}

// Book used to scroll through statuses
type ConversationBook struct {
	Current []Conversation
	Book
}

// Updates the book to the values from the previous page
func (book *ConversationBook) Previous() []Conversation {
	if book.HasPrevious() {
		return book.Update(book.prevUrl)
	} else {
		return book.Current
	}
}

// Updates the book to the values from the next page
func (book *ConversationBook) Next() []Conversation {
	if book.HasNext() {
		return book.Update(book.nextUrl)
	} else {
		return book.Current
	}
}

// Returns all available statuses.
// Warning: Can take a long time (receives a maximum of 80 accounts per second)
func (book *ConversationBook) All() []Conversation {
	var curr []Conversation
	curr = append(curr, book.Current...)
	var savepoint = book.currentUrl
	for book.HasPrevious() {
		accs := book.Previous()
		if accs != nil && len(accs) > 0 {
			curr = append(curr, accs...)
			time.Sleep(time.Second)
		} else {
			break
		}
	}
	if savepoint != "" {
		book.Update(savepoint)
		time.Sleep(time.Second)
		for book.HasNext() {
			accs := book.Next()
			if accs != nil && len(accs) > 0 {
				curr = append(curr, accs...)
				time.Sleep(time.Second)
			} else {
				break
			}
		}
	}
	book.Current = curr
	return book.Current
}

// Updates the book using the values retrieved from the given url
func (book *ConversationBook) Update(url string) []Conversation {
	val, prev, next, err := book.Get(url, book.accessToken)
	if err != nil {
		log.Fatal(err)
	}
	book.Current = val
	book.currentUrl = url
	book.prevUrl = prev
	book.nextUrl = next
	return book.Current
}

// Retrieves the page of data and link-urls from the given url
func (book *ConversationBook) Get(url, accessToken string) ([]Conversation, string, string, error) {
	var acc []Conversation
	headers, _, err := apiCall(url, http.MethodGet, accessToken, nil, &acc)
	if err != nil {
		return nil, "", "", err
	}
	prev, next := readLinkHeader(headers)
	return acc, prev, next, nil
}

//endregion
