package gotodonr

import "net/http"

// Gets all follow suggestions
//
// Returns a slice of suggested accounts
func GetFollowSuggestions(baseUrl, accessToken string) ([]Account, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/suggestions")
	var accs []Account
	_, _, err := apiCall(requestUrl, http.MethodGet, accessToken, nil, &accs)
	if err != nil {
		return nil, err
	}
	return accs, nil
}

// Deletes a follow suggestion
//
//	accountId: the account whose follow suggestion should be deleted
func DeleteFollowSuggestion(baseUrl, accessToken, accountId string) error {
	requestUrl := constructUrl(baseUrl, "/api/v1/suggestions/"+accountId)
	_, _, err := apiCall(requestUrl, http.MethodDelete, accessToken, nil, nil)
	if err != nil {
		return err
	}
	return nil
}
