package gotodonr

import (
	"net/http"
)

// Gets a page of accounts the user has blocked.
//
//	limit: maximum amount of results per page
//
// Returns a page of accounts the user has blocked and uris pointing to the previous and next page.
func GetAccountBlocks(baseUrl, accessToken string, limit int) ([]Account, string, string, error) {
	data := map[string]interface{}{}
	if limit > 0 {
		data["limit"] = getLimit(limit)
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/blocks", data)
	var accs []Account
	headers, _, err := apiCall(requestUrl, http.MethodGet, accessToken, nil, &accs)
	if err != nil {
		return nil, "", "", err
	}
	prev, next := readLinkHeader(headers)
	return accs, next, prev, nil
}

// Gets a book of accounts the user has blocked.
//
//	limit: maximum amount of results per page
//
// Returns an AccountBook which can be used to scroll through the pages of blocked accounts.
func GetAccountBlocksBook(baseUrl, accessToken string, limit int) AccountBook {
	data := map[string]interface{}{}
	if limit > 0 {
		data["limit"] = getLimit(limit)
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/blocks", data)
	var book = NewAccountBook(accessToken)
	book.Update(requestUrl)
	return book
}

// Blocks the given account.
//
//	accountId: the accountId associated with the account to be blocked
//
// Returns the updated relationship
func BlockAccount(baseUrl, accessToken, accountId string) (Relationship, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/accounts/"+accountId+"/block")
	var rel Relationship
	_, _, err := apiCall(requestUrl, http.MethodPost, accessToken, nil, &rel)
	if err != nil {
		return Relationship{}, err
	}
	return rel, nil
}

// Unblocks the given account
//
//	accountId: the accountId associated with the account to be blocked
//
// Returns the updated relationship
func UnblockAccount(baseUrl, accessToken, accountId string) (Relationship, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/accounts/"+accountId+"/unblock")
	var rel Relationship
	_, _, err := apiCall(requestUrl, http.MethodPost, accessToken, nil, &rel)
	if err != nil {
		return Relationship{}, err
	}
	return rel, nil
}
