package gotodonr

import (
	"fmt"
	"strings"
)

const (
	ScopeWrite              string = "write"
	ScopeWriteAccounts      string = "write:accounts"
	ScopeWriteBlocks        string = "write:blocks"
	ScopeWriteConversations string = "write:conversations"
	ScopeWriteFavourites    string = "write:favourites"
	ScopeWriteFilters       string = "write:filters"
	ScopeWriteFollows       string = "write:follows"
	ScopeWriteLists         string = "write:lists"
	ScopeWriteMedia         string = "write:media"
	ScopeWriteMutes         string = "write:mutes"
	ScopeWriteNotifications string = "write:notifications"
	ScopeWriteReports       string = "write:reports"
	ScopeWriteStatuses      string = "write:statuses"
	ScopeRead               string = "read"
	ScopeReadAccounts       string = "read:accounts"
	ScopeReadBlocks         string = "read:blocks"
	ScopeReadFavourites     string = "read:favourites"
	ScopeReadFilters        string = "read:filters"
	ScopeReadFollows        string = "read:follows"
	ScopeReadLists          string = "read:lists"
	ScopeReadMutes          string = "read:mutes"
	ScopeReadNotifications  string = "read:notifications"
	ScopeReadSearch         string = "read:search"
	ScopeReadStatuses       string = "read:statuses"
	ScopeFollow             string = "follow"
	ScopePush               string = "push"
	ScopeAdminRead          string = "admin:read"
	ScopeAdminReadAccounts  string = "admin:read:accounts"
	ScopeAdminReadReports   string = "admin:read:reports"
	ScopeAdminWrite         string = "admin:write"
	ScopeAdminWriteAccounts string = "admin:write:accounts"
	ScopeAdminWriteReports  string = "admin:write:reports"

	VisibilityPublic   string = "public"   // Visibility used when posting a status
	VisibilityUnlisted string = "unlisted" // Visibility used when posting a status
	VisibilityPrivate  string = "private"  // Visibility used when posting a status
	VisibilityDirect   string = "direct"   // Visibility used when posting a status

	AttachmentUnknown string = "unknown" // Type of the attachment of a media post
	AttachmentImage   string = "image"   // Type of the attachment of a media post
	AttachmentGifv    string = "gifv"    // Type of the attachment of a media post
	AttachmentVideo   string = "video"   // Type of the attachment of a media post

	CardLink  string = "link"  // Type of status card
	CardPhoto string = "photo" // Type of status card
	CardVideo string = "video" // Type of status card
	CardRich  string = "rich"  // Type of status card

	ContextHome          string = "home"          // Context of a timeline
	ContextNotifications string = "notifications" // Context of a timeline
	ContextPublic        string = "public"        // Context of a timeline
	ContextThread        string = "thread"        // Context of a timeline

	DurationMinute     int = 60                  // Shortcut for a duration of a minute
	DurationHalfHour   int = 30 * DurationMinute // Shortcut for a duration of thirty minutes
	DurationHour       int = 60 * DurationMinute // Shortcut for a duration of an hour
	DurationQuarterDay int = 6 * DurationHour    // Shortcut for a duration of six hours
	DurationHalfDay    int = 12 * DurationHour   // Shortcut for a duration of twelve hours
	DurationDay        int = 24 * DurationHour   // Shortcut for a duration of twenty-four hours
	DurationWeek       int = 7 * DurationDay     // Shortcut for a duration of seven days

	NotificationFollow    string = "follow"    // Type of notifications to receive through the push service
	NotificationMention   string = "mention"   // Type of notifications to receive through the push service
	NotificationReblog    string = "reblog"    // Type of notifications to receive through the push service
	NotificationFavourite string = "favourite" // Type of notifications to receive through the push service
)

// Creates a comma-separated list of the selected scopes
func Scopes(permission ...string) string {
	return strings.Join(permission, " ")
}

// Creates a slice of contexts
func Contexts(context ...string) []string {
	return context
}

// Serializes a focal point
//
//	x: x-axis focal point, between 0.0 and 1.0
//	y: y-axis focal point, between 0.0 and 1.0
func FocalPoint(x, y float64) string {
	return fmt.Sprintf("%f,%f", x, y)
}
