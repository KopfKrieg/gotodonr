package gotodonr

import (
	"net/http"
)

// Gets a page of statuses in the home timeline
//
//	  maxId: only retrieve statuses before this status (optional)
//	sinceId: only retrieve statuses after this status (optional)
//	  minId: only retrieve statuses after this status (optional)
//	  limit: only retrieve this amount of statuses per page (default: 40)
//
// Returns a slice of statuses and two urls pointing to the previous and next page respectively
func GetHomeTimeline(baseUrl, accessToken, maxId, sinceId, minId string, limit int) ([]Status, string, string, error) {
	data := map[string]interface{}{
		"limit": getLimit(limit),
	}
	if maxId != "" {
		data["max_id"] = maxId
	}
	if sinceId != "" {
		data["since_id"] = sinceId
	}
	if minId != "" {
		data["min_id"] = minId
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/timelines/home", data)
	var stats []Status
	headers, _, err := apiCall(requestUrl, http.MethodGet, accessToken, nil, &stats)
	if err != nil {
		return nil, "", "", err
	}
	prev, next := readLinkHeader(headers)
	return stats, prev, next, nil
}

// Gets a book used to scroll through the pages of statuses in the home timeline
//
//	  limit: only retrieve this amount of statuses per page (default: 40)
//
// Returns a StatusBook used to scroll through the pages or collect them all at once
func GetHomeTimelineBook(baseUrl, accessToken string, limit int) StatusBook {
	data := map[string]interface{}{
		"limit": getLimit(limit),
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/timelines/home", data)
	var book = NewStatusBook(accessToken)
	book.Update(requestUrl)
	return book
}

// Gets a page of conversations
//
//	  maxId: only retrieve conversations with statuses before this status (optional)
//	sinceId: only retrieve conversations with statuses after this status (optional)
//	  minId: only retrieve conversations with statuses after this status (optional)
//	  limit: only retrieve this amount of statuses per page (default: 40)
//
// Returns a slice of conversations and two urls pointing to the previous and next page respectively
func GetConversations(baseUrl, accessToken, maxId, sinceId, minId string, limit int) ([]Conversation, string, string, error) {
	data := map[string]interface{}{
		"limit": getLimit(limit),
	}
	if maxId != "" {
		data["max_id"] = maxId
	}
	if sinceId != "" {
		data["since_id"] = sinceId
	}
	if minId != "" {
		data["min_id"] = minId
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/conversations", data)
	var cons []Conversation
	headers, _, err := apiCall(requestUrl, http.MethodGet, accessToken, nil, &cons)
	if err != nil {
		return nil, "", "", err
	}
	prev, next := readLinkHeader(headers)
	return cons, prev, next, nil
}

// Gets a book used to scroll through the pages of conversations
//
//	  limit: only retrieve this amount of statuses per page (default: 40)
//
// Returns a ConversationBook used to scroll through the pages or collect them all at once
func GetConversationsBook(baseUrl, accessToken string, limit int) ConversationBook {
	data := map[string]interface{}{
		"limit": getLimit(limit),
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/conversations", data)
	var book = NewConversationBook(accessToken)
	book.Update(requestUrl)
	return book
}

// Gets a page of statuses in the public timeline
//
//      local: only retrieve local statuses (default: false)
//	onlyMedia: only retrieve statuses with attachments (default: false)
//	    maxId: only retrieve statuses before this status (optional)
//	  sinceId: only retrieve statuses after this status (optional)
//	    minId: only retrieve statuses after this status (optional)
//	    limit: only retrieve this amount of statuses per page (default: 40)
//
// Returns a slice of statuses and two urls pointing to the previous and next page respectively
func GetPublicTimeline(baseUrl, accessToken string, local, onlyMedia bool, maxId, sinceId, minId string, limit int) ([]Status, string, string, error) {
	data := map[string]interface{}{
		"local":      local,
		"only_media": onlyMedia,
		"limit":      getLimit(limit),
	}
	if maxId != "" {
		data["max_id"] = maxId
	}
	if sinceId != "" {
		data["since_id"] = sinceId
	}

	if minId != "" {
		data["min_id"] = minId
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/timelines/public", data)
	var stats []Status
	headers, _, err := apiCall(requestUrl, http.MethodGet, accessToken, nil, &stats)
	if err != nil {
		return nil, "", "", err
	}
	prev, next := readLinkHeader(headers)
	return stats, prev, next, nil
}

// Gets a book used to scroll through the pages of statuses in the public timeline
//
//      local: only retrieve local statuses (default: false)
//	onlyMedia: only retrieve statuses with attachments (default: false)
//	    limit: only retrieve this amount of statuses per page (default: 40)
//
// Returns a StatusBook used to scroll through the pages or collect them all at once
func GetPublicTimelineBook(baseUrl, accessToken string, local, onlyMedia bool, limit int) StatusBook {
	data := map[string]interface{}{
		"local":      local,
		"only_media": onlyMedia,
		"limit":      getLimit(limit),
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/timelines/public", data)
	var book = NewStatusBook(accessToken)
	book.Update(requestUrl)
	return book
}

// Gets a page of statuses in a hashtags timeline
//
//	  hashTag: the hashtag whose timeline to retrieve
//      local: only retrieve local statuses (default: false)
//	onlyMedia: only retrieve statuses with attachments (default: false)
//	    maxId: only retrieve statuses before this status (optional)
//	  sinceId: only retrieve statuses after this status (optional)
//	    minId: only retrieve statuses after this status (optional)
//	    limit: only retrieve this amount of statuses per page (default: 40)
//
// Returns a slice of statuses and two urls pointing to the previous and next page respectively
func GetHashtagTimeline(baseUrl, accessToken, hashTag string, local, onlyMedia bool, maxId, sinceId, minId string, limit int) ([]Status, string, string, error) {
	data := map[string]interface{}{
		"local":      local,
		"only_media": onlyMedia,
		"limit":      getLimit(limit),
	}
	if maxId != "" {
		data["max_id"] = maxId
	}
	if sinceId != "" {
		data["since_id"] = sinceId
	}
	if minId != "" {
		data["min_id"] = minId
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/timelines/tag/"+hashTag, data)
	var stats []Status
	headers, _, err := apiCall(requestUrl, http.MethodGet, accessToken, nil, &stats)
	if err != nil {
		return nil, "", "", err
	}
	prev, next := readLinkHeader(headers)
	return stats, prev, next, nil
}

// Gets a book used to scroll through the pages of statuses in a hashtags timeline
//
//	  hashTag: the hashtag whose timeline to retrieve
//      local: only retrieve local statuses (default: false)
//	onlyMedia: only retrieve statuses with attachments (default: false)
//	    limit: only retrieve this amount of statuses per page (default: 40)
//
// Returns a StatusBook used to scroll through the pages or collect them all at once
func GetHashtagTimelineBook(baseUrl, accessToken, hashTag string, local, onlyMedia bool, limit int) StatusBook {
	data := map[string]interface{}{
		"local":      local,
		"only_media": onlyMedia,
		"limit":      getLimit(limit),
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/timelines/tag/"+hashTag, data)
	var book = NewStatusBook(accessToken)
	book.Update(requestUrl)
	return book
}

// Gets a page of statuses in a lists timeline
//
//	   listId: identifier of the list whose timeline to retrieve
//	    maxId: only retrieve statuses before this status (optional)
//	  sinceId: only retrieve statuses after this status (optional)
//	    minId: only retrieve statuses after this status (optional)
//	    limit: only retrieve this amount of statuses per page (default: 40)
//
// Returns a slice of statuses and two urls pointing to the previous and next page respectively
func GetListTimeline(baseUrl, accessToken, listId, maxId, sinceId, minId string, limit int) ([]Status, string, string, error) {
	data := map[string]interface{}{
		"limit": getLimit(limit),
	}
	if maxId != "" {
		data["max_id"] = maxId
	}
	if sinceId != "" {
		data["since_id"] = sinceId
	}
	if minId != "" {
		data["min_id"] = minId
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/timelines/list/"+listId, data)
	var stats []Status
	headers, _, err := apiCall(requestUrl, http.MethodGet, accessToken, nil, &stats)
	if err != nil {
		return nil, "", "", err
	}
	prev, next := readLinkHeader(headers)
	return stats, prev, next, nil
}

// Gets a book used to scroll through the pages of statuses in a lists timeline
//
//	   listId: identifier of the list whose timeline to retrieve
//	    limit: only retrieve this amount of statuses per page (default: 40)
//
// Returns a StatusBook used to scroll through the pages or collect them all at once
func GetListTimelineBook(baseUrl, accessToken, listId string, limit int) StatusBook {
	data := map[string]interface{}{
		"limit": getLimit(limit),
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/timelines/list/"+listId, data)
	var book = NewStatusBook(accessToken)
	book.Update(requestUrl)
	return book
}
