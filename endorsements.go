package gotodonr

import (
	"net/http"
)

// Gets a page of accounts the user has endorsed (endorsed accounts are highlighted on the users profile)
//
//	limit: max amount of results per page
//
// Returns a slice of endorsed accounts and two urls pointing to the next and previous page respectively
func GetEndorsements(baseUrl, accessToken string, limit int) ([]Account, string, string, error) {
	data := map[string]interface{}{}
	if limit > 0 {
		data["limit"] = getLimit(limit)
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/endorsements", data)
	var accs []Account
	headers, _, err := apiCall(requestUrl, http.MethodGet, accessToken, nil, &accs)
	if err != nil {
		return nil, "", "", err
	}
	prev, next := readLinkHeader(headers)
	return accs, next, prev, nil
}

// Gets a book which can be used to scroll through pages of accounts the user has endorsed (endorsed accounts are highlighted on the users profile)
//
//	limit: max amount of results per page
//
// Returns an AccountBook which can be used to scroll through the pages or collect all pages at once
func GetEndorsementsBook(baseUrl, accessToken string, limit int) AccountBook {
	data := map[string]interface{}{}
	if limit > 0 {
		data["limit"] = getLimit(limit)
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/endorsements", data)
	var book = NewAccountBook(accessToken)
	book.Update(requestUrl)
	return book
}

// Endorses (pins) the given account
//
//	accountId: the account to endorse
//
// Returns the updated relationship
func EndorseAccount(baseUrl, accessToken, accountId string) (Relationship, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/accounts/"+accountId+"/pin")
	var rel Relationship
	_, _, err := apiCall(requestUrl, http.MethodPost, accessToken, nil, &rel)
	if err != nil {
		return Relationship{}, err
	}
	return rel, nil
}

// Unendorses (unpins) the given account
//
//	accountId: the account to unendorse
//
// Returns the updated relationship
func UnendorseAccount(baseUrl, accessToken, accountId string) (Relationship, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/accounts/"+accountId+"/unpin")
	var rel Relationship
	_, _, err := apiCall(requestUrl, http.MethodPost, accessToken, nil, &rel)
	if err != nil {
		return Relationship{}, err
	}
	return rel, nil
}
