package gotodonr

import (
	"log"
	"net/http"
)

// Gets a poll
//
//	pollId:  identifier of the poll to get
//
// Returns the poll
func GetPoll(baseUrl, pollId string) (Poll, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/polls/"+pollId)
	var poll Poll
	_, _, err := apiCall(requestUrl, http.MethodGet, "", nil, &poll)
	if err != nil {
		return Poll{}, err
	}
	return poll, nil
}

// Posts a vote on a poll
//
//	pollId: identifier of the poll to vote on
//	choices: one (or more) ordinals of choices to vote on (multiple ordinals can only be used when the vote allows multiple choices)
//
// Returns the updated poll
func VoteOnPoll(baseUrl, accessToken, pollId string, choices ...int) (Poll, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/polls/"+pollId+"/votes")
	var poll Poll
	_, body, err := apiCallJson(requestUrl, http.MethodPost, accessToken, map[string]interface{}{"choices": choices}, &poll)
	log.Println(body)
	if err != nil {
		return Poll{}, err
	}
	return poll, nil
}
