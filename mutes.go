package gotodonr

import (
	"net/http"
)

// Gets a page of muted accounts
//
//	limit: max amount of results per page (default: 40)
//
// Returns a slice of muted accounts and two urls pointing to the next and previous url respectively
func GetMutes(baseUrl, accessToken string, limit int) ([]Account, string, string, error) {
	data := map[string]interface{}{}
	if limit > 0 {
		data["limit"] = getLimit(limit)
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/mutes", data)
	var accs []Account
	headers, _, err := apiCall(requestUrl, http.MethodGet, accessToken, nil, &accs)
	if err != nil {
		return nil, "", "", err
	}
	prev, next := readLinkHeader(headers)
	return accs, prev, next, nil
}

// Gets a book used to scroll through pages of muted accounts
//
//	limit: max amount of results per page (default: 40)
//
// Returns an AccountBook used to scroll through the pages or collect them all at once
func GetMutesBook(baseUrl, accessToken string, limit int) AccountBook {
	data := map[string]interface{}{}
	if limit > 0 {
		data["limit"] = getLimit(limit)
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/mutes", data)
	var book = NewAccountBook(accessToken)
	book.Update(requestUrl)
	return book
}

// Mutes an account
//
// 	   accountId: identifier of the account to be muted
// notifications: if notifications for this account should be muted as well (default: true)
//
// Returns the updated relationship
func MuteAccount(baseUrl, accessToken, accountId string, notifications bool) (Relationship, error) {
	data := map[string]interface{}{
		"notifications": notifications,
	}
	requestUrl := constructUrl(baseUrl, "/api/v1/accounts/"+accountId+"/mute")
	var rel Relationship
	_, _, err := apiCallJson(requestUrl, http.MethodPost, accessToken, data, &rel)
	if err != nil {
		return Relationship{}, err
	}
	return rel, nil
}

// Unmutes an account
//
// 	   accountId: identifier of the account to be unmuted
//
// Returns the updated relationship
func UnmuteAccount(baseUrl, accessToken, accountId string) (Relationship, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/accounts/"+accountId+"/unmute")
	var rel Relationship
	_, _, err := apiCall(requestUrl, http.MethodPost, accessToken, nil, &rel)
	if err != nil {
		return Relationship{}, err
	}
	return rel, nil
}

// Mutes a status
//
// 	   statusId: identifier of the status to be muted
//
// Returns the status
func MuteStatus(baseUrl, accessToken, statusId string) (Status, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/statuses/"+statusId+"/mute")
	var sta Status
	_, _, err := apiCall(requestUrl, http.MethodPost, accessToken, nil, &sta)
	if err != nil {
		return Status{}, err
	}
	return sta, nil
}

// Unmutes a status
//
// 	   statusId: identifier of the status to be unmuted
//
// Returns the status
func UnmuteStatus(baseUrl, accessToken, statusId string) (Status, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/statuses/"+statusId+"/unmute")
	var sta Status
	_, _, err := apiCall(requestUrl, http.MethodPost, accessToken, nil, &sta)
	if err != nil {
		return Status{}, err
	}
	return sta, nil
}
