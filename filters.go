package gotodonr

import (
	"log"
	"net/http"
)

// Get all currently setup filters
//
// Returns a slice of filters
func GetFilters(baseUrl, accessToken string) ([]Filter, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/filters")
	var filters []Filter
	_, _, err := apiCall(requestUrl, http.MethodGet, accessToken, nil, &filters)
	if err != nil {
		return nil, err
	}
	return filters, nil
}

// Get a specific filter
//
//	filterId: identifier of the filter to be retrieved
//
// Returns the filter for the given id
func GetFilter(baseUrl, accessToken, filterId string) (Filter, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/filters/"+filterId)
	var filter Filter
	_, _, err := apiCall(requestUrl, http.MethodGet, accessToken, nil, &filter)
	if err != nil {
		return Filter{}, err
	}
	return filter, nil
}

// Creates a new filter
//
//	      phrase: the phrase to be checked for in statuses
//	     context: the context(s) in which the filter should be applied (one or more of home, notifications, public and/or thread)
//	irreversible: if true, matching statuses will be dropped from the timeline instead of hidden (only works with context home and notifications) (default: false)
//	   wholeWord: whether to consider word boundaries when matching (default: false)
//	   expiresIn: time in seconds when the filter will expire or no-expiration when set to 0 (default: 0)
//
// Returns the newly created filter
func CreateFilter(baseUrl, accessToken, phrase string, context []string, irreversible, wholeWord bool, expiresIn int) (Filter, error) {
	data := map[string]interface{}{
		"phrase":       phrase,
		"context":      context,
		"irreversible": irreversible,
		"whole_word":   wholeWord,
	}
	if expiresIn > 0 {
		data["expires_in"] = expiresIn
	}
	log.Println(data)
	requestUrl := constructUrl(baseUrl, "/api/v1/filters")
	var filter Filter
	_, _, err := apiCallJson(requestUrl, http.MethodPost, accessToken, data, &filter)
	if err != nil {
		return Filter{}, err
	}
	return filter, nil
}

// Updates an existing filter
//
//	      phrase: the phrase to be checked for in statuses
//	     context: the context(s) in which the filter should be applied (one or more of home, notifications, public and/or thread)
//	irreversible: if true, matching statuses will be dropped from the timeline instead of hidden (only works with context home and notifications) (default: false)
//	   wholeWord: whether to consider word boundaries when matching (default: false)
//	   expiresIn: time in seconds when the filter will expire or no-expiration when set to 0 (default: 0)
//
// Returns the updated filter
func UpdateFilter(baseUrl, accessToken, filterId, phrase string, context []string, irreversible, wholeWord bool, expiresIn int) (Filter, error) {
	data := map[string]interface{}{
		"phrase":       phrase,
		"context":      context,
		"irreversible": irreversible,
		"whole_word":   wholeWord,
	}
	if expiresIn > 0 {
		data["expires_in"] = expiresIn
	}
	requestUrl := constructUrl(baseUrl, "/api/v1/filters/"+filterId)
	var filter Filter
	_, _, err := apiCallJson(requestUrl, http.MethodPut, accessToken, data, &filter)
	if err != nil {
		return Filter{}, err
	}
	return filter, nil
}

// Removes a filter
//
//	filterId: identifier of the filter to be deleted
func DeleteFilter(baseUrl, accessToken, filterId string) error {
	requestUrl := constructUrl(baseUrl, "/api/v1/filters/"+filterId)
	_, _, err := apiCall(requestUrl, http.MethodDelete, accessToken, nil, nil)
	if err != nil {
		return err
	}
	return nil
}
