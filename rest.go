package gotodonr

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"html"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"reflect"
	"regexp"
	"strconv"
	"strings"
)

// Perform an API call with url-encoded data
//
//	 requestUrl: api endpoint to query
//	     method: http method to use (GET, POST, PUT, PATCH, DELETE, ...)
//	accessToken: the access token to authenticate the request with (optional)
//	       data: key-value-data which should be encoded and sent as body (optional)
//          out: struct which should be updated with received data (optional)
//
// Returns any errors, headers and the answer body
func apiCall(requestUrl, method, accessToken string, data url.Values, out interface{}) (http.Header, string, error) {
	return apiCallWithHeader(requestUrl, method, accessToken, data, nil, out)
}

// Perform an API call with url-encoded data and additional headers
//
//	 requestUrl: api endpoint to query
//	     method: http method to use (GET, POST, PUT, PATCH, DELETE, ...)
//	accessToken: the access token to authenticate the request with (optional)
//	       data: key-value-data which should be encoded and sent as body (optional)
//	    headers: key-value-data which should be set as headers
//          out: struct which should be updated with received data (optional)
//
// Returns any errors, headers and the answer body
func apiCallWithHeader(requestUrl, method, accessToken string, data url.Values, headers map[string]string, out interface{}) (http.Header, string, error) {
	var req *http.Request
	var err error
	var bdy string

	if data == nil {
		req, err = http.NewRequest(method, requestUrl, nil)
	} else {
		bdy = data.Encode()
		req, err = http.NewRequest(method, requestUrl, strings.NewReader(bdy))
	}
	if err != nil {
		return nil, "", err
	}

	if headers != nil {
		for k, v := range headers {
			req.Header.Set(k, v)
		}
	}

	if accessToken != "" {
		req.Header.Set("Authorization", "Bearer "+accessToken)
	}

	if data != nil {
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		req.Header.Set("Content-Length", strconv.Itoa(len(bdy)))
	}

	return do(req, out)
}

// Perform an API call with json-encoded data
//
//	 requestUrl: api endpoint to query
//	     method: http method to use (GET, POST, PUT, PATCH, DELETE, ...)
//	accessToken: the access token to authenticate the request with (optional)
//	       data: key-value-data which should be encoded and sent as body (optional)
//          out: struct which should be updated with received data (optional)
//
// Returns any errors, headers and the answer body
func apiCallJson(requestUrl, method, accessToken string, data map[string]interface{}, out interface{}) (http.Header, string, error) {
	return apiCallJsonWithHeaders(requestUrl, method, accessToken, data, nil, out)
}

// Perform an API call with json-encoded data and additional headers
//
//	 requestUrl: api endpoint to query
//	     method: http method to use (GET, POST, PUT, PATCH, DELETE, ...)
//	accessToken: the access token to authenticate the request with (optional)
//	       data: key-value-data which should be encoded and sent as body (optional)
//	    headers: key-value-data which should be set as headers
//          out: struct which should be updated with received data (optional)
//
// Returns any errors, headers and the answer body
func apiCallJsonWithHeaders(requestUrl, method, accessToken string, data map[string]interface{}, headers map[string]string, out interface{}) (http.Header, string, error) {
	var req *http.Request
	var err error
	var bdy string

	if data == nil {
		req, err = http.NewRequest(method, requestUrl, nil)
	} else {
		byt, err := json.Marshal(data)
		if err != nil {
			return nil, "", err
		}
		bdy := string(byt)
		req, err = http.NewRequest(method, requestUrl, strings.NewReader(bdy))
	}
	if err != nil {
		return nil, "", err
	}

	if headers != nil {
		for k, v := range headers {
			req.Header.Set(k, v)
		}
	}

	if accessToken != "" {
		req.Header.Set("Authorization", "Bearer "+accessToken)
	}

	if data != nil {
		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("Content-Length", strconv.Itoa(len(bdy)))
	}

	return do(req, out)
}

// Perform an API call with multipart/form-data-encoded data
//
//	 requestUrl: api endpoint to query
//	     method: http method to use (GET, POST, PUT, PATCH, DELETE, ...)
//	accessToken: the access token to authenticate the request with (optional)
//    fileParam: name of the header-field which will contain the file (optional)
//	   filePath: path to the file which should be uploaded (optional)
//	       data: key-value-data which should be encoded and sent as body (optional)
//          out: struct which should be updated with received data (optional)
//
// Returns any errors, headers and the answer body
func apiCallFile(requestUrl, method, accessToken, fileParam, filePath string, data map[string]string, out interface{}) (http.Header, string, error) {
	return apiCallFileWithHeaders(requestUrl, method, accessToken, fileParam, filePath, data, nil, out)
}

// Perform an API call with multipart/form-data-encoded data and additional headers
//
//	 requestUrl: api endpoint to query
//	     method: http method to use (GET, POST, PUT, PATCH, DELETE, ...)
//	accessToken: the access token to authenticate the request with (optional)
//    fileParam: name of the header-field which will contain the file (optional)
//	   filePath: path to the file which should be uploaded (optional)
//	       data: key-value-data which should be encoded and sent as body (optional)
//	    headers: key-value-data which should be set as headers
//          out: struct which should be updated with received data (optional)
//
// Returns any errors, headers and the answer body
func apiCallFileWithHeaders(requestUrl, method, accessToken, fileParam, filePath string, data map[string]string, headers map[string]string, out interface{}) (http.Header, string, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, "", err
	}
	defer file.Close()

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile(fileParam, filepath.Base(filePath))
	if err != nil {
		return nil, "", err
	}
	_, err = io.Copy(part, file)
	if data != nil {
		for key, val := range data {
			_ = writer.WriteField(key, val)
		}
	}
	err = writer.Close()
	if err != nil {
		return nil, "", err
	}

	req, err := http.NewRequest(method, requestUrl, body)
	if err != nil {
		return nil, "", err
	}

	if headers != nil {
		for k, v := range headers {
			req.Header.Set(k, v)
		}
	}

	req.Header.Set("Content-Type", writer.FormDataContentType())
	if accessToken != "" {
		req.Header.Set("Authorization", "Bearer "+accessToken)
	}
	return do(req, out)
}

// Gets an url (used with the prev/next urls returned by functions with pagination)
//
//	         url: the url to get
//	accessToken: the access token to authenticate the request with (optional)
//          out: struct which should be updated with received data (optional)
//
// Returns any errors, headers, the answer body and two urls pointing to the previous and next page respectively
func GetUrl(url, accessToken string, out interface{}) (http.Header, string, string, string, error) {
	var req *http.Request
	var err error

	req, err = http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, "", "", "", err
	}

	if accessToken != "" {
		req.Header.Set("Authorization", "Bearer "+accessToken)
	}

	headers, body, err := do(req, out)
	prev, next := readLinkHeader(headers)
	return headers, body, prev, next, err
}

var regexpParseHtmlErrors = regexp.MustCompile("<h1>(.+)[\\s\\S]*</h1>")

// Executes a request, retrieves data and processes it (filtering errors and decoding json data)
//
//	req: the request to execute
//	out: struct which should be updated with received data (optional)
//
// Returns any errors, headers and the answer body
func do(req *http.Request, out interface{}) (http.Header, string, error) {
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, "", err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return resp.Header, "", err
	}

	bodyStr := string(body)

	if strings.HasPrefix(bodyStr, "<!DOCTYPE html>") {
		htmlStr := html.UnescapeString(bodyStr)
		htmlByt := []byte(htmlStr)
		if regexpParseHtmlErrors.Match(htmlByt) {
			return resp.Header, bodyStr, errors.New(string(regexpParseHtmlErrors.FindSubmatch(htmlByt)[1]))
		} else {
			return resp.Header, bodyStr, errors.New(html.UnescapeString(htmlStr))
		}
	}

	var apiError = Error{}
	err = json.Unmarshal(body, &apiError)
	if apiError.Error != "" {
		return resp.Header, bodyStr, errors.New(apiError.Error)
	}

	if out != nil {
		err = json.Unmarshal(body, out)
		if err != nil {
			return resp.Header, "", err
		}
	}

	return resp.Header, bodyStr, err
}

// URL-Encodes a key-value-pair and adds it to a set of url.Values
//
//	params: the set of data to add to
// 	   key: the key of the parameter
//	   val: the value of the parameter
func encode(params *url.Values, key string, val interface{}) {
	// Check the type of val (since slices and maps have to be serialized in a special way, the "bracket notation".
	// There is no special handling for booleans implemented since fmt.Sprint(...) already returns them as correct strings.
	// See https://docs.joinmastodon.org/api/parameters/
	if val == nil {
		params.Set(key, "")
	} else if isArrayOrSlice(val) {
		encodeArray(params, key, val)
	} else if isMap(val) {
		encodeMap(params, key, val)
	} else {
		params.Set(key, fmt.Sprint(val))
	}
}

// Check if the given object is an array (using reflection)
func isArrayOrSlice(obj interface{}) bool {
	return reflect.TypeOf(obj).Kind() == reflect.Array || reflect.TypeOf(obj).Kind() == reflect.Slice
}

// Check if the given object is a map (using reflection)
func isMap(obj interface{}) bool {
	return reflect.TypeOf(obj).Kind() == reflect.Map
}

// Encodes an array using the bracket notation and adds it to the parameters
func encodeArray(params *url.Values, key string, val interface{}) {
	reVal := reflect.ValueOf(val)
	for i := 0; i < reVal.Len(); i++ {
		// Parameters are set with key as "arrayName[index]" and value as "value"
		params.Set(fmt.Sprint(key, "[", i, "]"), fmt.Sprint(reVal.Index(i).Interface()))
	}
}

// Encodes a map using the bracket notation and adds it to the parameters
func encodeMap(params *url.Values, key string, val interface{}) {
	iter := reflect.ValueOf(val).MapRange()
	for iter.Next() {
		// Parameters are set with key as "mapName[key]" and value as "value"
		params.Set(fmt.Sprint(key, "[", iter.Key(), "]"), fmt.Sprint(iter.Value()))
	}
}

// ConstructUrl performs basic checks on the given baseUrl & endpoint and concatenates them
func constructUrl(baseUrl, endpoint string) string {
	if !strings.HasPrefix(baseUrl, "http") {
		baseUrl = "https://" + baseUrl
	}

	if !strings.HasPrefix(endpoint, "/") {
		endpoint = "/" + endpoint
	}
	return baseUrl + endpoint
}

// ConstructData creates a url.Values map from the given map and encodes all values to strings
func constructData(arguments map[string]interface{}) url.Values {
	params := url.Values{}
	for k, v := range arguments {
		encode(&params, k, v)
	}
	return params
}

// Constructs an url for GET requests
func constructGetUrl(baseUrl, endpoint string, arguments map[string]interface{}) string {
	return constructUrl(baseUrl, endpoint) + "?" + constructData(arguments).Encode()
}

var regexpParseHeaderLinks = regexp.MustCompile("<([^>]*)>; rel=\"([^\"]*)\"")

// Extracts the urls for the previous/next page from the "Link" header (if there is one)
func readLinkHeader(headers http.Header) (string, string) {
	prev := ""
	next := ""
	if headers != nil && headers.Get("Link") != "" {
		submatches := regexpParseHeaderLinks.FindAllStringSubmatch(headers.Get("Link"), -1)
		for _, submatch := range submatches {
			if submatch[2] == "next" {
				next = submatch[1]
			} else if submatch[2] == "prev" {
				prev = submatch[1]
			}
		}
	}
	return prev, next
}
