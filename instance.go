package gotodonr

import "net/http"

// Gets information about a server
//
// Returns an instance
func GetInstance(baseUrl string) (Instance, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/instance")
	var in Instance
	_, _, err := apiCall(requestUrl, http.MethodGet, "", nil, &in)
	if err != nil {
		return Instance{}, err
	}
	return in, nil
}
