package gotodonr

import (
	"net/http"
)

// Get all lists created by the user
//
// Returns a slice of lists
func GetLists(baseUrl, accessToken string) ([]List, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/lists")
	var lists []List
	_, _, err := apiCall(requestUrl, http.MethodGet, accessToken, nil, &lists)
	if err != nil {
		return nil, err
	}
	return lists, nil
}

// Get a single list created by the user
//
//	listId: identifier of the list to retrieve
//
// Returns a list
func GetList(baseUrl, accessToken, listId string) (List, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/lists/"+listId)
	var list List
	_, _, err := apiCall(requestUrl, http.MethodGet, accessToken, nil, &list)
	if err != nil {
		return List{}, err
	}
	return list, nil
}

// Get all lists containing an account
//
//	accountId: identifier for an account the lists should be queried for
//
// Returns a slice of lists containing the account
func GetListsWithAccount(baseUrl, accessToken, accountId string) ([]List, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/accounts/"+accountId+"/lists")
	var lists []List
	_, _, err := apiCall(requestUrl, http.MethodGet, accessToken, nil, &lists)
	if err != nil {
		return nil, err
	}
	return lists, nil
}

// Gets a page of accounts in a list
//
//	listId: identifier for the list to retrieve
// 	 limit: max amount of results per page (default: 0)
//
// Returns a slice of accounts and two urls pointing to the previous and next page respectively
func ListAccounts(baseUrl, accessToken, listId string, limit int) ([]Account, string, string, error) {
	data := map[string]interface{}{}
	// According to docs, supports "0" to return all accounts (So far apparently the only function that supports it)
	if limit > 0 {
		data["limit"] = getLimit(limit)
	} else if limit == 0 {
		data["limit"] = 0
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/lists/"+listId+"/accounts", data)
	var accs []Account
	headers, _, err := apiCall(requestUrl, http.MethodGet, accessToken, nil, &accs)
	if err != nil {
		return nil, "", "", err
	}
	prev, next := readLinkHeader(headers)
	return accs, prev, next, nil
}

// Gets a book used to scroll through the pages of accounts in a list
//
//	listId: identifier for the list to retrieve
// 	 limit: max amount of results per page (default: 0)
//
// Returns an AccountBook used to scroll through the pages or collect all at once
func ListAccountsBook(baseUrl, accessToken, listId string, limit int) AccountBook {
	data := map[string]interface{}{}
	// According to docs, supports "0" to return all accounts (So far apparently the only function that supports it)
	if limit > 0 {
		data["limit"] = getLimit(limit)
	} else if limit == 0 {
		data["limit"] = 0
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/lists/"+listId+"/accounts", data)
	var book = NewAccountBook(accessToken)
	book.Update(requestUrl)
	return book
}

// Creates a new list
//
//	title: title of the new list
//
// Returns the newly created list
func CreateList(baseUrl, accessToken, title string) (List, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/lists")
	var list List
	_, _, err := apiCallJson(requestUrl, http.MethodPost, accessToken, map[string]interface{}{"title": title}, &list)
	if err != nil {
		return List{}, err
	}
	return list, nil
}

// Updates a list
//
//	listId: identifier of the list to be updated
//	 title: the new title of the list
//
// Returns the updated list
func UpdateList(baseUrl, accessToken, listId, title string) (List, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/lists/"+listId)
	var list List
	_, _, err := apiCallJson(requestUrl, http.MethodPut, accessToken, map[string]interface{}{"title": title}, &list)
	if err != nil {
		return List{}, err
	}
	return list, nil
}

// Deletes the list
//
//	listId: identifier of the list to be updated
func DeleteList(baseUrl, accessToken, listId string) error {
	requestUrl := constructUrl(baseUrl, "/api/v1/lists/"+listId)
	_, _, err := apiCall(requestUrl, http.MethodDelete, accessToken, nil, nil)
	if err != nil {
		return err
	}
	return nil
}

// Adds the accounts to the list
//
//	  listId: identifier of the list to be updated
//	accounts: slice of accounts which should be added to the list
func AddAccountsToList(baseUrl, accessToken, listId string, accounts ...string) error {
	requestUrl := constructUrl(baseUrl, "/api/v1/lists/"+listId+"/accounts")
	_, _, err := apiCallJson(requestUrl, http.MethodPost, accessToken, map[string]interface{}{"account_ids": accounts}, nil)
	if err != nil {
		return err
	}
	return nil
}

// Removes the accounts from the list
//
//	  listId: identifier of the list to be updated
//	accounts: slice of accounts which should be removed from the list
func RemoveAccountsFromList(baseUrl, accessToken, listId string, accounts ...string) error {
	requestUrl := constructUrl(baseUrl, "/api/v1/lists/"+listId+"/accounts")
	_, _, err := apiCallJson(requestUrl, http.MethodDelete, accessToken, map[string]interface{}{"account_ids": accounts}, nil)
	if err != nil {
		return err
	}
	return nil
}
