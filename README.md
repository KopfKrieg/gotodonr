# Gotodon - REST

Gotodon REST is the foundation of Gotodon. It implements all REST calls on which other parts of Gotodon build upon.
Since these calls can be very repetitive (i.e. the first two arguments are almost always baseUrl and accessToken) and
everything (loading, storing, ...) has to be done by you, you can use Gotodon - Managed instead.

All repositories which Gotodon consists of:
- [Gotodon REST](https://gitlab.com/Cyphrags/gotodonr)
- [Gotodon](https://gitlab.com/Cyphrags/gotodon)
- [Gotodon WebSocket](https://gitlab.com/Cyphrags/gotodonws)
- [Gotodon Examples](https://gitlab.com/Cyphrags/gotodon-examples)

# Features
- [OAuth](https://docs.joinmastodon.org/api/authentication/)
    - Authorization Code Flow
    - Password Grant Flow
    - Client Credentials Flow
    - Verifying Application/Access Tokens
    - Revoking Access Tokens
- All API endpoints as of July 2019

# Getting Started
This section will cover everything necessary to get started, from registering a client to getting an access token 
using any of the three available flows (Authorization Code Flow, Password Grant Flow and Client Credentials Flow)

We start off by declaring constants and globals which will be frequently used in api calls.

```go
const (
    // The address which points to the mastodon instance you want to access
    baseUrl = "domain.tld"
)

// The app, which holds the data used to identify the app, authorize it or receive push notifications.
// It contains the clientId, which identifies your client and is used for any token functions
// and the clientSecret, which is used to make sure it really is your client
// (typically used to create signatures of requests)
var app App

// The token which holds your accessToken, which is used to send authenticated requests. It is linked to
// both the app (through the clientId) and the account which authorized it.
// Warning: There is also a type of accessToken which is not linked to an account,
// which is used when using the Client Credentials Flow (where a client creates its own
// account instead of using an existing one). These tokens can't use account-based
// functions, like posting statuses and such (but can be used to get a regular accessToken).
var token Token
```

## Registering the client

First we need to register this client as an app.

```go
err, app = RegisterApp(
    // The baseUrl, as stated above, is the address of the server to connect to
    baseUrl,

    // The clientName is displayed on statuses on the bottom right (when the status is opened)
    "Gotodon",

    // If a website is set, a user can click on the clientName in a status and it will take him to it.
    "https://gitlab.com/Cyphrags/gotodon-examples/blob/master/rest_example.go",

    // The scopes set here are the "request boundaries", i.e. they are not yet granted but can limit
    // which scopes can be requested when authorizing the client.
    Scopes(ScopeRead, ScopeWrite, ScopeFollow, ScopePush),

    // Here you can set optional redirect uris, which again only limit which redirect uris can be set when
    // authorizing the client. You can leave this empty (even without the empty string) to use the default
    // "no redirect" (urn:ietf:wg:oauth:2.0:oob).
    "")
```

## Authorizing the client

Now that we have registered the app, we need to authorize it. This will grant us an [Access Token](https://www.oauth.com/oauth2-servers/access-tokens/)
which will tells the server who is calling and which account the call will effect (i.e. who is posting the status or 
which profile to update with this nice, new avatar image).

There are three ways to authorize your client:
 1. [Authorization Code Flow (ACF)](https://gitlab.com/Cyphrags/gotodonr#authorization-code-flow)
 2. [Password Grant Flow (PGF)](https://gitlab.com/Cyphrags/gotodonr#password-grant-flow)
 3. [Client Credentials Flow (CCF)](https://gitlab.com/Cyphrags/gotodonr#client-credentials-flow)
 
ACF and PGF require that an account already exists. Additionally, ACF requires the user to confirm the app (which 
removes the to pass any password around which makes it ultimately safer). CCF will create a new account.

> These might not be official/commonly used abbreviations. I used them here for simplicities sake. 

### Scopes

All authorization methods require a set of scopes which will restrict which functions the client can call. You can 
find a list of scopes in [gotodonr/constants.go](https://gitlab.com/Cyphrags/gotodonr/blob/master/constants.go) 
(and other constants too, like Visibility for statuses).
> You should only request the scopes you really need rather than requesting everything. Doing so can reduce
> the damage caused if an access token is leaked or stolen.
 
### Authorization Code Flow

In the [Authorization Code Flow](https://oauth.net/2/grant-types/authorization-code/) we first generate an URL which the user has to open (this website is the OAuth web
endpoint of the mastodon server). The user then needs to confirm that he wants to authorize the app which will 
present him with an authorization code (using the redirect uri and a local webserver, the authorization code can
also be retrieved automatically after the user hit confirm, instead of having them enter it somewhere).

```go
authorizeUrl := GetAuthorizeUrl(
    baseUrl,
    app.ClientId,

    // This is where we actually request the scope for the client. These scopes are limited by the
    // scopes that were set when registering the client and can't exceed them (i.e. it is possible to request
    // ScopeReadAccounts here when you set ScopeRead when registering the client, but not vice versa)
    Scopes(ScopeRead, ScopeWrite, ScopeFollow, ScopePush),

    // The redirect uri can be used to redirect the authorization code to a different address, like a local
    // http server which could automatically read it out instead of having the user enter it.
    "")

// This placeholder variable needs to be filled with the authorization code and how that happens is up to you.
var authorizationCode string
```

After the user confirmed the authorization request and the authorization code was entered/retrieved, we simply 
exchange it for an access token (note that the scope used in ```GetAuthorizeUrl()``` and here need to be the same):

```go
err, token = GetAccessTokenFromAuthorizationCode(
    baseUrl,
    app.ClientId,
    app.ClientSecret,
    authorizationCode,

    // These scopes need to match the scopes used in GetAuthorizeUrl(...)
    Scopes(ScopeRead, ScopeWrite, ScopeFollow, ScopePush),

    // The redirect uri can be used to redirect the accessToken to a different website (or to a local server
    // again) if you want to handle things differently. Leaving it empty will output the accessToken directly
    // and our variable t will hold onto it.
    "")
```

### Password Grant Flow

In the [Password Grant Flow](https://oauth.net/2/grant-types/password/) we use a login-style flow to get an access token, which means that we simply send 
the username and password to the server to get an access token. 

> Requiring a user to login is a potential security risk and/or might scare off some users. 
> In any case, the Authorization Code Flow is preferable (and I strongly advice you to use it).

```go
err, token = GetAccessTokenFromLogin(
    baseUrl,
    app.ClientId,
    app.ClientSecret,

    // The username used to login (usually the e-mail address).
    // How you get the username is up to you (could be hardcoded, through environment variables or by
    // asking the user through a (web-)interface, popup, console, ...)
    username,

    // The password used to login.
    // How you get the password is up to you (could be hardcoded, through environment variables or by
    // asking the user through a (web-)interface, popup, console, ...)
    password,

    // This is where we actually request the scope for the client. These scopes are limited by the
    // scopes that were set when registering the client and can't exceed them (i.e. it is possible to request
    // ScopeReadAccounts here when you set ScopeRead when registering the client, but not vice versa)
    Scopes(ScopeRead, ScopeWrite, ScopeFollow, ScopePush),

    // The redirect uri can be used to redirect the accessToken to a different website (or to a local server
    // again) if you want to handle things differently. Leaving it empty will output the accessToken directly
    // and our variable t will hold onto it.
    "")
```

### Client Credentials Flow

In the [Client Credentials Flow](https://oauth.net/2/grant-types/client-credentials/) we first get a special access token which we then use to create a new account
and then get an actual access token for the account we created.

```go
err, token = GetAccessTokenFromCredentials(
    baseUrl,
    app.ClientId,
    app.ClientSecret,

    // This is where we actually request the scope for the client. These scopes are limited by the
    // scopes that were set when registering the client and can't exceed them (i.e. it is possible to request
    // ScopeReadAccounts here when you set ScopeRead when registering the client, but not vice versa)
    Scopes(ScopeRead, ScopeWrite, ScopeFollow, ScopePush))
```

> This token is not linked to an account and can't be used in any account related methods. 

Now we use this token to create a new account.

```go
err, token = CreateAccount(
    baseUrl,

    // The accessToken we got from GetAccessTokenFromCredentials()
    token.AccessToken,

    // The UserName which is used to "@" this account (@UserName@domain.tld)
    // This is not the displayName, which can always be changed.
    // The userName, however, can only be changed by moving the account.
    username,

    // The email address used for this account. It will receive the initial "confirm your account" email.
    email,

    // The password used to login (in case the client needs to login again or the user wants to login
    // through the web interface, app or another client).
    password,

    // Basically needs to be true and confirms that you agree with the terms and conditions.
    true,

    // The locale which affects the "confirm your account" email
    "en")
```

> ``CreateAccount`` might throw an "you can't access this page" error if user registration is disabled on
> the server

### Revoking an Access Token

If you ever need to revoke an access token (i.e. the client is deleted/uninstalled or gets a brand new token) you can
simply use the function ```RevokeToken```:

```go
err = RevokeToken(baseUrl, app.ClientId, app.ClientSecret, token.AccessToken)
```

## Verifying the client

Now that we have setup the client, we can finally do something with it. To test that everything went alright,
we might simply post a status which will tell the world that everything is fine:

```go
PostTextStatus(baseUrl, token.AccessToken, "MyClient is working!", "", false, "", VisibilityPublic, "en", "")
```

> You can view the full example here: [gotodon-examples/rest_example.go](https://gitlab.com/Cyphrags/gotodon-examples/blob/master/rest_example.go)