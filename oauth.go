package gotodonr

import "net/http"

// Construct an url used to get an authorization code which can be exchanged for an
// access token using GetAccessTokenFromAuthorizationCode.
//
//	      scope: space-separated list of scopes to be authorized for
//	redirectUri: uri the user will be redirected to, which will have the access token attached as url param (default: urn:ietf:wg:oauth:2.0:oob)
//
// Returns the url the user needs to open in order to get an authorization code
func GetAuthorizeUrl(baseUrl, clientId, scope, redirectUri string) string {
	if redirectUri == "" {
		redirectUri = "urn:ietf:wg:oauth:2.0:oob"
	}
	return constructGetUrl(baseUrl, "/oauth/authorize", map[string]interface{}{
		"response_type": "code",
		"client_id":     clientId,
		"redirect_uri":  redirectUri,
		"scope":         scope,
	})
}

// Exchanges an authorization code for an access token
//
//	authorizationCode: the authorization code received from the user
//	            scope: space-separated list of scopes to be authorized for
//	      redirectUri: uri the user will be redirected to, which will have the access token attached as url param (default: urn:ietf:wg:oauth:2.0:oob)
//
// Returns a new token
func GetAccessTokenFromAuthorizationCode(baseUrl, clientId, clientSecret, authorizationCode, scope, redirectUri string) (Token, error) {
	if redirectUri == "" {
		redirectUri = "urn:ietf:wg:oauth:2.0:oob"
	}
	requestUrl := constructUrl(baseUrl, "/oauth/token")
	data := map[string]interface{}{
		"grant_type":    "authorization_code",
		"client_id":     clientId,
		"client_secret": clientSecret,
		"scope":         scope,
		"code":          authorizationCode,
		"redirect_uri":  redirectUri,
	}
	var token Token
	_, _, err := apiCallJson(requestUrl, http.MethodPost, "", data, &token)
	if err != nil {
		return Token{}, err
	}
	return token, nil
}

// Gets a token using login credentials
//
//     username: username to log in with
//     password: password to log in with
//        scope: space-separated list of scopes to be authorized for
//	redirectUri: uri the user will be redirected to, which will have the access token attached as url param (default: urn:ietf:wg:oauth:2.0:oob)
//
// Returns a new token
func GetAccessTokenFromLogin(baseUrl, clientId, clientSecret, username, password, scope, redirectUri string) (Token, error) {
	if redirectUri == "" {
		redirectUri = "urn:ietf:wg:oauth:2.0:oob"
	}
	requestUrl := constructUrl(baseUrl, "/oauth/token")
	data := map[string]interface{}{
		"grant_type":    "password",
		"client_id":     clientId,
		"client_secret": clientSecret,
		"username":      username,
		"password":      password,
		"scope":         scope,
		"redirect_uri":  redirectUri,
	}
	var token Token
	_, _, err := apiCallJson(requestUrl, http.MethodPost, "", data, &token)
	if err != nil {
		return Token{}, err
	}
	return token, nil
}

// Gets a token by authorizing the application using its client credentials.
//
//	scope: space-separated list of scopes to be authorized for
//
// Returns a new token
//
// Note: the token received from this method is an application token, not an account token. It can be used for functions
// which require an authentication, but will fail for all functions requiring a user (like posting a status, ...).
func GetAccessTokenFromCredentials(baseUrl, clientId, clientSecret, scope string) (Token, error) {
	requestUrl := constructUrl(baseUrl, "/oauth/token")
	data := map[string]interface{}{
		"grant_type":    "client_credentials",
		"client_id":     clientId,
		"client_secret": clientSecret,
		"scope":         scope,
	}
	var token Token
	_, _, err := apiCallJson(requestUrl, http.MethodPost, "", data, &token)
	if err != nil {
		return Token{}, err
	}
	return token, nil
}

// Revokes an access token
func RevokeToken(baseUrl, clientId, clientSecret, accessToken string) error {
	requestUrl := constructUrl(baseUrl, "/oauth/revoke")
	data := map[string]interface{}{
		"client_id":     clientId,
		"client_secret": clientSecret,
		"token":         accessToken,
	}
	_, _, err := apiCallJson(requestUrl, http.MethodPost, "", data, nil)
	if err != nil {
		return err
	}
	return nil
}
