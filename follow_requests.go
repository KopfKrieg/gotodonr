package gotodonr

import (
	"net/http"
)

// Returns a single page containing open follower requests
//
//	limit: maximum amount of accounts to retrieve (default: 40)
//
// Returns a slice of accounts with a pending follow request and two urls pointing to the next and previous page respectively
func GetFollowRequests(baseUrl, accessToken string, limit int) ([]Account, string, string, error) {
	data := map[string]interface{}{}
	if limit > 0 {
		data["limit"] = getLimit(limit)
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/follow_requests", data)
	var accs []Account
	headers, _, err := apiCall(requestUrl, http.MethodGet, accessToken, nil, &accs)
	if err != nil {
		return nil, "", "", err
	}
	prev, next := readLinkHeader(headers)
	return accs, prev, next, nil
}

// Gets a book which can be used to scroll through pages of open follower requests
//
//	    limit: maximum amount of accounts to retrieve per page (default: 40)
//
// Returns an AccountBook which can be used to scroll through the pages or collect all pages at once
func GetFollowRequestsBook(baseUrl, accessToken string, limit int) AccountBook {
	data := map[string]interface{}{}
	if limit > 0 {
		data["limit"] = getLimit(limit)
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/follow_requests", data)
	var book = NewAccountBook(accessToken)
	book.Update(requestUrl)
	return book
}

// Accepts a follower request
//
//	accountId: identifier of the account whose follow request should be accepted
func AuthorizeFollowRequest(baseUrl, accessToken, accountId string) error {
	requestUrl := constructUrl(baseUrl, "/api/v1/follow_requests/"+accountId+"/authorize")
	_, _, err := apiCall(requestUrl, http.MethodPost, accessToken, nil, nil)
	if err != nil {
		return err
	}
	return nil
}

// Rejects a follower request
//
//	accountId: identifier of the account whose follow request should be rejected
func RejectFollowRequest(baseUrl, accessToken, accountId string) error {
	requestUrl := constructUrl(baseUrl, "/api/v1/follow_requests/"+accountId+"/reject")
	_, _, err := apiCall(requestUrl, http.MethodPost, accessToken, nil, nil)
	if err != nil {
		return err
	}
	return nil
}
