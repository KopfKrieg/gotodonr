package gotodonr

import (
	"net/http"
)

// Uploads a new media object which can be used as attachment
//
//	   filePath: path to the file which should be uploaded
//	description: description of the file (for screen readers and loading errors) (optional, max 420 characters))
//	     focusX: x-axis focal point, between 0.0 and 1.0 (default: 0.0)
//	     focusY: y-axis focal point, between 0.0 and 1.0 (default: 0.0)
//
// Returns the newly created attachment
func UploadMedia(baseUrl, accessToken, filePath, description string, focusX, focusY float64) (Attachment, error) {
	data := map[string]string{
		"focus": FocalPoint(focusX, focusY),
	}
	if description != "" {
		data["description"] = description
	}
	requestUrl := constructUrl(baseUrl, "/api/v1/media")
	var att Attachment
	_, _, err := apiCallFile(requestUrl, http.MethodPost, accessToken, "file", filePath, data, &att)
	if err != nil {
		return Attachment{}, err
	}
	return att, nil
}

// Updates an existing attachment
//
//	description: description of the file (for screen readers and loading errors) (optional, max 420 characters))
//	     focusX: x-axis focal point, between 0.0 and 1.0 (default: 0.0)
//	     focusY: y-axis focal point, between 0.0 and 1.0 (default: 0.0)
//
// Returns the updated attachment
func UpdateMedia(baseUrl, accessToken, attachmentId, description string, focusX, focusY float64) (Attachment, error) {
	data := map[string]interface{}{
		"focus": FocalPoint(focusX, focusY),
	}
	if description != "" {
		data["description"] = description
	}
	requestUrl := constructUrl(baseUrl, "/api/v1/media/"+attachmentId)
	var att Attachment
	_, _, err := apiCallJson(requestUrl, http.MethodPut, accessToken, data, &att)
	if err != nil {
		return Attachment{}, err
	}
	return att, nil
}
