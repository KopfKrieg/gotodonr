package gotodonr

// The app retrieved when registering a client at a mastodon server
type App struct {
	Id           string `json:"id"`            // The identifier of the app
	ClientName   string `json:"name"`          // The client name as shown in statuses and applications view
	Website      string `json:"website"`       // The website which provides more information on the client
	RedirectUri  string `json:"redirect_uri"`  // The OAuth redirect endpoint
	ClientId     string `json:"client_id"`     // The OAuth ClientId
	ClientSecret string `json:"client_secret"` // The OAuth ClientSecret
	VapidKey     string `json:"vapid_key"`     // The associated accounts VAPID key used by WebPush
}

// A struct used when uploading media attachments within the status post call
type Media struct {
	Path        string  // Path to the media file
	Description string  // A description of the media (optional)
	FocusX      float64 // The x-axis focus point (between 0.0 and 1.0)
	FocusY      float64 // The y-axis focus point (between 0.0 and 1.0)
}

// An account of a user, with profile data and statistics
type Account struct {
	Id             string   `json:"id"`              // Local identifier of the account
	Username       string   `json:"username"`        // Username the person signed up with (used when @-ing: @Username)
	Account        string   `json:"acct"`            // Full account name (username@server.tld) or Username if the account belongs to the queried server
	DisplayName    string   `json:"display_name"`    // Display Name which can be edited by the user. If empty, use Username instead
	Locked         bool     `json:"locked"`          // Following a locked account will send a follow request (which has to be accepted) instead of directly following the account
	CreatedAt      string   `json:"created_at"`      // The timestamp at which the account was created
	FollowersCount int      `json:"followers_count"` // Amount of accounts following the account
	FollowingCount int      `json:"following_count"` // Amount of accounts the account follows
	StatusesCount  int      `json:"statuses_count"`  // Amount of statuses the account posted
	Url            string   `json:"url"`             // URL pointing to the profile of the account
	Avatar         string   `json:"avatar"`          // URL pointing to the image used as avatar by the account
	AvatarStatic   string   `json:"avatar_static"`   // URL pointing to the static image used as avatar by the account
	Header         string   `json:"header"`          // URL pointing to the image used as header by the account
	HeaderStatic   string   `json:"header_static"`   // URL pointing to the static image used as header by the account
	Source         Source   `json:"source"`          // Source data of the account
	Emojis         []Emoji  `json:"emojis"`          // Custom emojis from the account
	Moved          *Account `json:"moved"`           // A reference to the original account if the account was moved
	Bot            bool     `json:"bot"`             // True if the account is a bot
}

// A field shown on a users profile (up to four fields can be set)
type Field struct {
	Name       string `json:"name"`        // The name of the field, shown on the left side
	Value      string `json:"value"`       // The value of the field, shown on the right side
	VerifiedAt string `json:"verified_at"` // Timestamp of when the field was verified (or empty if not verified)
}

// Source data of an account
type Source struct {
	Privacy   string  `json:"privacy"`   // The default set visibility of statuses (public, private, unlisted, direct)
	Sensitive bool    `json:"sensitive"` // The default sensitivity setting of statuses
	Language  string  `json:"language"`  // The default language of statuses
	Note      string  `json:"note"`      // Description/Biography of the account
	Fields    []Field `json:"fields"`    // Array of up to four fields shown on the profile
}

// Token data retrieved when authorizing a client
type Token struct {
	AccessToken string `json:"access_token"` // The OAuth access token
	TokenType   string `json:"token_type"`   // The type of token (normally Bearer)
	Scope       string `json:"scope"`        // Scope of the token
	CreatedAt   int64  `json:"created_at"`   // Timestamp of when the token was generated
}

// Display data of the application used to post a status
type Application struct {
	Name    string `json:"name"`    // Name of the application
	Website string `json:"website"` // Website of the application
}

// Data of an attachment which can be added to a status
type Attachment struct {
	Id          string `json:"id"`          // The local identifier of the attachment
	Type        string `json:"type"`        // The type of attachment (unknown, image, gifv or video)
	Url         string `json:"url"`         // Url pointing to the attachment on the server
	RemoteUrl   string `json:"remote_url"`  // Url pointing to the attachment on the original server (if the attachment was federated)
	PreviewUrl  string `json:"preview_url"` // Url pointing to a small preview version of the attachment
	TextUrl     string `json:"text_url"`    // Url pointing to the text version of the attachment
	Meta        Meta   `json:"meta"`        // Metadata of the attachment
	Description string `json:"description"` // A description of the attachment
}

// Metadata of an attachment
type Meta struct {
	Focus    Focus          `json:"focus"`    // Focus point of an attachment
	Small    MetaAttributes `json:"small"`    // Set of attributes for the small (preview) version of an attachment
	Original MetaAttributes `json:"original"` // Set of attributes for the original version of an attachment
}

// Focus data of an image, used to align the preview image
type Focus struct {
	X float32 `json:"x"` // X-Axis focal point of an attachment (between 0.0 and 1.0)
	Y float32 `json:"y"` // Y-Axis focal point of an attachment (between 0.0 and 1.0)
}

// Metadata of an attachment
type MetaAttributes struct {
	Width     float32 `json:"width"`      // Width of an attachment
	Height    float32 `json:"height"`     // Height of an attachment
	Size      string  `json:"size"`       // Size of an image attachment
	Aspect    float32 `json:"aspect"`     // Aspect ratio of an image attachment
	FrameRate string  `json:"frame_rate"` // Framerate of a gifv/video attachment
	Duration  float32 `json:"duration"`   // Duration of a gifv/video attachment
	Bitrate   float32 `json:"bitrate"`    // Bitrate of a gifv/video attachment
}

// Card data used to generate a preview/embedding for links in statuses
type Card struct {
	Url          string  `json:"url"`           // Url the card is pointing
	Title        string  `json:"title"`         // Title of the card
	Description  string  `json:"description"`   // Description of the card
	Image        string  `json:"image"`         // Preview image for the url shown in the card
	Type         string  `json:"type"`          // The type of card (link, photo, video or rich)
	AuthorName   string  `json:"author_name"`   // Name of the author of the linked article/embeddable
	AuthorUrl    string  `json:"author_url"`    // Website of the author of the linked article/embeddable
	ProviderName string  `json:"provider_name"` // Name of the provider hosting the article/embeddable
	ProviderUrl  string  `json:"provider_url"`  // Website of the provider hosting the linked article/embeddable
	Html         string  `json:"html"`          // Html used when the card contains an embeddable
	Width        float32 `json:"width"`         // Width of the preview image
	Height       float32 `json:"height"`        // Height of the preview image
}

// The context of a status, used in conversations
type Context struct {
	Ancestors   []Status `json:"ancestors"`   // Statuses preceding this status
	Descendants []Status `json:"descendants"` // Statuses following a status
}

// Data used for custom emojis
type Emoji struct {
	ShortCode       string `json:"shortcode"`         // ShortCode of the emoji (used when 'writing' emojis, i.e. ':+1:' of which '+1' is the ShortCode of the thumbs-up emoji)
	StaticUrl       string `json:"static_url"`        // Url pointing to the static image of the emoji
	Url             string `json:"url"`               // Url pointing to the image of the emoji
	VisibleInPicker bool   `json:"visible_in_picker"` // True if the emoji should be visible in pickers
}

// Data sent by the server to provide information why an action/request failed
type Error struct {
	Error string `json:"error"` // Contains the error
}

// Data for a filter used to automatically hide statuses from users
type Filter struct {
	Id           string   `json:"id"`           // Identifier of the filter
	Phrase       string   `json:"phrase"`       // Phrase that is filtered
	Context      []string `json:"context"`      // Context the filter should be applied in (one or more of home, notifications, public and/or thread)
	ExpiresAt    string   `json:"expires_at"`   // Timestamp at which the filter expires
	Irreversible bool     `json:"irreversible"` // If the filter is irreversible, the filtered content will be deleted from the personal view instead of hidden. Therefore it can't be shown even if the filter is deleted or timed out.
	WholeWord    bool     `json:"whole_word"`   // True if the filter needs to match a whole word instead of matching anything
}

// Data used to provide information about a mastodon server
type Instance struct {
	Uri            string   `json:"uri"`             // Uri of the mastodon instance
	Title          string   `json:"title"`           // Title of the mastodon instance
	Description    string   `json:"description"`     // Description of the mastodon instance
	Email          string   `json:"email"`           // Public contact email of the mastodon instance
	Version        string   `json:"version"`         // Version of the mastodon instance
	Thumbnail      string   `json:"thumbnail"`       // Url pointing to the thumbnail (preview) image of the mastodon instance
	Urls           URLs     `json:"urls"`            // Url pointing to the streaming api endpoint (Used by WebSockets)
	Stats          Stats    `json:"stats"`           // Statistics of the mastodon instance
	Languages      []string `json:"languages"`       // Official languages of the mastodon instance
	ContactAccount Account  `json:"contact_account"` // Administrator/official contact of the mastodon instance
}

// Data pointing to a endpoint which can be used by WebSockets
type URLs struct {
	StreamingAPI string `json:"streaming_api"` // Url pointing to the streaming api endpoint (Used by WebSockets)
}

// Statistics for a mastodon server
type Stats struct {
	UserCount   int64 `json:"user_count"`   // Amount of users registered at the mastodon instance
	StatusCount int64 `json:"status_count"` // Amount of statuses posted by members of the mastodon instance
	DomainCount int64 `json:"domain_count"` // Amount of domains federated on the mastodon instance
}

// Lists allow the creation of timelines which only contains statuses by a specified set of accounts
type List struct {
	Id    string `json:"id"`    // Identifier for the list
	Title string `json:"title"` // Title of the list
}

// Data of a mentioned account
type Mention struct {
	Url      string `json:"url"`      // Url pointing to the url of the mentioned account
	Username string `json:"username"` // Username of the mentioned account
	Account  string `json:"acct"`     // Full account name (username@server.tld) or Username if the account belongs to the queried server
	Id       string `json:"id"`       // Identifier of the mentioned account
}

// Data of a notification
type Notification struct {
	Id        string  `json:"id"`         // Identifier for the notification
	Type      string  `json:"type"`       // Type of notification (follow, mention, reblog or favourite)
	CreatedAt string  `json:"created_at"` // Timestamp of when the notification was generated
	Account   Account `json:"account"`    // The account which performed an action (followed, mentioned, reblogged or favourited)
	Status    Status  `json:"status"`     // The status which an action was performed on/with
}

// Data of a poll
type Poll struct {
	Id         string       `json:"id"`          // Identifier of the poll
	ExpiresAt  string       `json:"expires_at"`  // Timestamp of when the poll will expire/expired at
	Expired    bool         `json:"expired"`     // True if the poll already expired
	Multiple   bool         `json:"multiple"`    // True if the poll allows multiple-choice answers
	VotesCount int          `json:"votes_count"` // Amount of votes already posted (counts multiple-choice answers as multiple votes)
	Options    []PollOption `json:"options"`     // Options to choose from
	Voted      bool         `json:"voted"`       // True if the user already voted on this poll
}

// Data for a single choice in a poll
type PollOption struct {
	Title      string `json:"title"`       // Title of the choice
	VotesCount int    `json:"votes_count"` // Amount of votes for the choice
}

// Data for the push subscription
type PushSubscription struct {
	Id        int64  `json:"id"`         // Identifier of the subscription
	Endpoint  string `json:"endpoint"`   // Endpoint of the subscription
	ServerKey string `json:"server_key"` // ServerKey used for the subscription
	Alerts    Alerts `json:"alerts"`     // Alerts which are subscribed to
}

// Set of alerts which a subscription can be subscribed to
// TODO: Should be booleans, but server returns JSON strings
type Alerts struct {
	Follow    string `json:"follow"`    // Notify when receiving a new follower
	Favourite string `json:"favourite"` // Notify when a status is favourited
	Reblog    string `json:"reblog"`    // Notify when a status is reblogged
	Mention   string `json:"mention"`   // Notify when the user is mentioned
	Poll      string `json:"poll"`      // Notify on poll updates
}

// Relationship data between two accounts
type Relationship struct {
	Id                  string `json:"id"`                   // Identifier of the account
	Following           bool   `json:"following"`            // True if the user follows the account
	FollowedBy          bool   `json:"followed_by"`          // True if the account follows the user
	Blocking            bool   `json:"blocking"`             // True if the user blocks the account
	Muting              bool   `json:"muting"`               // True if the user muted the account
	MutingNotifications bool   `json:"muting_notifications"` // True if the user muted notifications for the account
	Requested           bool   `json:"requested"`            // True if the user sent a follow request to the account
	DomainBlocking      bool   `json:"domain_blocking"`      // True if the account is on a blocked domain
	ShowingReblogs      bool   `json:"showing_reblogs"`      // True if the user wants to see reblogs by the account
	Endorsed            bool   `json:"endorsed"`             // True if the user endorsed the account
}

// Set of search results
type Result struct {
	Accounts []Account `json:"accounts"` // Accounts matching the query
	Statuses []Status  `json:"statuses"` // Statuses matching the query
	Hashtags []Tag     `json:"hashtags"` // Tags matching the query
}

// Data of a status
type Status struct {
	Id                 string       `json:"id"`                     // Identifier for the status
	Uri                string       `json:"uri"`                    // Uri pointing to the status
	Url                string       `json:"url"`                    // Url pointing to the status
	Account            Account      `json:"account"`                // Account which posted the status
	InReplyToId        string       `json:"in_reply_to_id"`         // Identifier of the status replied to
	InReplyToAccountId string       `json:"in_reply_to_account_id"` // Identifier of the account replied to
	Reblog             *Status      `json:"reblog"`                 // Status which was reblogged
	Content            string       `json:"content"`                // Content of the status
	CreatedAt          string       `json:"created_at"`             // Timestamp of when the status was created
	Emojis             []Emoji      `json:"emojis"`                 // Set of emojis used in the status
	RepliesCount       int          `json:"replies_count"`          // Amount of replies given to the status
	ReblogsCount       int          `json:"reblogs_count"`          // Amount of times the status was reblogged
	FavouritesCount    int          `json:"favourites_count"`       // Amount of favourites the status received
	Reblogged          bool         `json:"reblogged"`              // True if the user reblogged the status
	Favourited         bool         `json:"favourited"`             // True if the user favourited the status
	Muted              bool         `json:"muted"`                  // True if the user muted the status
	Sensitive          bool         `json:"sensitive"`              // True if the content of the status should be marked sensitive
	SpoilerText        string       `json:"spoiler_text"`           // Text shown if the status is sensitive
	Visibility         string       `json:"visibility"`             // Visibility of the status (public, private, unlisted or direct)
	MediaAttachments   []Attachment `json:"media_attachments"`      // Attachments of the status
	Mentions           []Mention    `json:"mentions"`               // Other accounts mentioned in the status
	Tags               []Tag        `json:"tags"`                   // Tags in the status
	Card               Card         `json:"card"`                   // Card data of the status
	Poll               Poll         `json:"poll"`                   // Poll data of the status
	Application        Application  `json:"application"`            // Application which posted the status
	Language           string       `json:"language"`               // Language of the content
	Pinned             bool         `json:"pinned"`                 // True if the status is pinned
}

// Data of a scheduled status
type ScheduledStatus struct {
	Id               string       `json:"id"`                // Identifier for the scheduled status
	ScheduledAt      string       `json:"scheduled_at"`      // Timestamp at which the status will be posted
	Params           StatusParams `json:"params"`            // Parameters of the status which will be posted
	MediaAttachments []Attachment `json:"media_attachments"` // Attachments of the status which will be posted
}

// Data of a status that is yet to be posted
type StatusParams struct {
	Text          string   `json:"text"`           // Content of the status
	InReplyToId   string   `json:"in_reply_to_id"` // Identifier of the status it replies to
	MediaIds      []string `json:"media_ids"`      // Identifiers of the attachments added to the status
	Sensitive     string   `json:"sensitive"`      // True if the status will be sensitive
	SpoilerText   string   `json:"spoiler_text"`   // Text shown if the status is sensitive
	Visibility    string   `json:"visibility"`     // Visibility of the status (public, private, unlisted or direct)
	ScheduledAt   string   `json:"scheduled_at"`   // Timestamp at which the status will be posted
	ApplicationId int64    `json:"application_id"` // Identifier of the application which scheduled the status
}

// Data of a (hash) tag
type Tag struct {
	Name    string    `json:"name"`    // Name of the (hash) tag
	Url     string    `json:"url"`     // Url pointing to a timeline of statuses containing the (hash) tag
	History []History `json:"history"` // Historic data of uses for the (hash) tag
}

// A dated statistic of the uses for a (hash) tag
type History struct {
	Day      string `json:"day"`      // Timestamp of the date this entry is referring to
	Uses     string `json:"uses"`     // Times the (hash) tag has been used at that day
	Accounts string `json:"accounts"` // Amount of different accounts who used the (hash) tag
}

// Data of a conversation
type Conversation struct {
	Id         string    `json:"id"`          // Identifier of this conversation
	Accounts   []Account `json:"accounts"`    // Accounts participating in the conversation
	LastStatus Status    `json:"last_status"` // The status which was posted last
	Unread     bool      `json:"unread"`      // True if the conversation has unread statuses
}
