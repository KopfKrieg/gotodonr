package gotodonr

import (
	"net/http"
)

// Get all scheduled statuses
//
// Returns a slice of scheduled statuses
func GetScheduledStatuses(baseUrl, accessToken string) ([]ScheduledStatus, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/scheduled_statuses")
	var stat []ScheduledStatus
	_, _, err := apiCall(requestUrl, http.MethodGet, accessToken, nil, &stat)
	if err != nil {
		return nil, err
	}
	return stat, nil
}

// Gets a scheduled status
//
//	scheduledId: identifier of the scheduled status to get
//
// Returns the scheduled status
func GetScheduledStatus(baseUrl, accessToken, scheduledId string) (ScheduledStatus, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/scheduled_statuses/"+scheduledId)
	var stat ScheduledStatus
	_, _, err := apiCall(requestUrl, http.MethodGet, accessToken, nil, &stat)
	if err != nil {
		return ScheduledStatus{}, err
	}
	return stat, nil
}

// Schedules a new status
//
//	        status: the text of the status
//	   scheduledAt: timestamp of when the status will be posted
//	   inReplyToId: identifier of the status it will reply to (optional)
//	      mediaIds: identifiers of attachments (media status)
//	   pollOptions: options for the poll (poll status)
//	 pollExpiresIn: time in seconds the poll will be live (poll status) (default: DurationDay)
// 	  pollMultiple: set to true to allow multiple choices (poll status) (default: false)
// 	pollHideTotals: set to true to hide the results while the poll is live (poll status) (default: false)
// 	     sensitive: set to true if the post contains sensitive content (default: false)
//	   spoilerText: text which will be shown instead if the status is marked as sensitive (optional)
//	    visibility: visibility of the status (either public, private, unlisted or direct) (default: public)
//	      language: the language of the status (ISO 8601) (optional)
//	idempotencyKey: helps preventing duplicate posts (optional)
//
// Returns the newly scheduled status
func ScheduleStatus(baseUrl, accessToken, status, scheduledAt, inReplyToId string, mediaIds []string, pollOptions []string, pollExpiresIn int, pollMultiple, pollHideTotals, sensitive bool, spoilerText, visibility, language, idempotencyKey string) (ScheduledStatus, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/statuses")
	data := map[string]interface{}{
		"status":       status,
		"scheduled_at": scheduledAt,
		"sensitive":    sensitive,
		"spoiler_text": spoilerText,
		"visibility":   visibility,
		"language":     language,
	}

	if inReplyToId != "" {
		data["in_reply_to_id"] = inReplyToId
	}

	if mediaIds != nil && len(mediaIds) > 0 {
		data["media_ids"] = mediaIds
	}

	if pollOptions != nil {
		pollData := map[string]interface{}{
			"options":     pollOptions,
			"expires_in":  pollExpiresIn,
			"multiple":    pollMultiple,
			"hide_totals": pollHideTotals,
		}
		data["poll"] = pollData
	}

	headers := map[string]string{}
	if idempotencyKey != "" {
		headers["Idempotency-Key"] = idempotencyKey
	}

	var stat ScheduledStatus
	_, _, err := apiCallJsonWithHeaders(requestUrl, http.MethodPost, accessToken, data, headers, &stat)
	if err != nil {
		return ScheduledStatus{}, err
	}
	return stat, nil
}

// Schedules a new text status
//
//	        status: the text of the status
//	   scheduledAt: timestamp of when the status will be posted
//	   inReplyToId: identifier of the status it will reply to (optional)
// 	     sensitive: set to true if the post contains sensitive content (default: false)
//	   spoilerText: text which will be shown instead if the status is marked as sensitive (optional)
//	    visibility: visibility of the status (either public, private, unlisted or direct) (default: public)
//	      language: the language of the status (ISO 8601) (optional)
//	idempotencyKey: helps preventing duplicate posts (optional)
//
// Returns the newly scheduled status
func ScheduleTextStatus(baseUrl, accessToken, status, scheduledAt, inReplyToId string, sensitive bool, spoilerText, visibility, language, idempotencyKey string) (ScheduledStatus, error) {
	return ScheduleStatus(baseUrl, accessToken, status, scheduledAt, inReplyToId, nil, nil, 0, false, false, sensitive, spoilerText, visibility, language, idempotencyKey)
}

// Schedules a new media status
//
//	        status: the text of the status
//	   scheduledAt: timestamp of when the status will be posted
//	   inReplyToId: identifier of the status it will reply to (optional)
//	      mediaIds: identifiers of attachments (media status)
// 	     sensitive: set to true if the post contains sensitive content (default: false)
//	   spoilerText: text which will be shown instead if the status is marked as sensitive (optional)
//	    visibility: visibility of the status (either public, private, unlisted or direct) (default: public)
//	      language: the language of the status (ISO 8601) (optional)
//	idempotencyKey: helps preventing duplicate posts (optional)
//
// Returns the newly scheduled status
func ScheduleMediaStatus(baseUrl, accessToken, status, scheduledAt, inReplyToId string, mediaIds []string, sensitive bool, spoilerText, visibility, language, idempotencyKey string) (ScheduledStatus, error) {
	return ScheduleStatus(baseUrl, accessToken, status, scheduledAt, inReplyToId, mediaIds, nil, 0, false, false, sensitive, spoilerText, visibility, language, idempotencyKey)
}

// Uploads any attachments and schedules a new media status
//
//	        status: the text of the status
//	   scheduledAt: timestamp of when the status will be posted
//	   inReplyToId: identifier of the status it will reply to (optional)
//	        medias: slice of medias which should be uploaded and attached (optional)
// 	     sensitive: set to true if the post contains sensitive content (default: false)
//	   spoilerText: text which will be shown instead if the status is marked as sensitive (optional)
//	    visibility: visibility of the status (either public, private, unlisted or direct) (default: public)
//	      language: the language of the status (ISO 8601) (optional)
//	idempotencyKey: helps preventing duplicate posts (optional)
//
// Returns the newly scheduled status
func UploadAndScheduleMediaStatus(baseUrl, accessToken, status, scheduledAt, inReplyToId string, medias []Media, sensitive bool, spoilerText, visibility, language, idempotencyKey string) (ScheduledStatus, error) {
	var mediaIds []string
	for _, media := range medias {
		att, err := UploadMedia(baseUrl, accessToken, media.Path, media.Description, media.FocusX, media.FocusY)
		if err != nil {
			return ScheduledStatus{}, err
		}
		mediaIds = append(mediaIds, att.Id)
	}
	return ScheduleMediaStatus(baseUrl, accessToken, status, scheduledAt, inReplyToId, mediaIds, sensitive, spoilerText, visibility, language, idempotencyKey)
}

// Schedules a new poll
//
//	        status: the text of the status
//	   scheduledAt: timestamp of when the status will be posted
//	   inReplyToId: identifier of the status it will reply to (optional)
//	   pollOptions: options for the poll (poll status)
//	 pollExpiresIn: time in seconds the poll will be live (poll status) (default: DurationDay)
// 	  pollMultiple: set to true to allow multiple choices (poll status) (default: false)
// 	pollHideTotals: set to true to hide the results while the poll is live (poll status) (default: false)
// 	     sensitive: set to true if the post contains sensitive content (default: false)
//	   spoilerText: text which will be shown instead if the status is marked as sensitive (optional)
//	    visibility: visibility of the status (either public, private, unlisted or direct) (default: public)
//	      language: the language of the status (ISO 8601) (optional)
//	idempotencyKey: helps preventing duplicate posts (optional)
//
// Returns the newly scheduled status
func SchedulePollStatus(baseUrl, accessToken, status, scheduledAt, inReplyToId string, pollOptions []string, pollExpiresIn int, pollMultiple, pollHideTotals, sensitive bool, spoilerText, visibility, language, idempotencyKey string) (ScheduledStatus, error) {
	return ScheduleStatus(baseUrl, accessToken, status, scheduledAt, inReplyToId, nil, pollOptions, pollExpiresIn, pollMultiple, pollHideTotals, sensitive, spoilerText, visibility, language, idempotencyKey)
}

// Moves a scheduled status to a new time
//
//	scheduledId: identifier of the scheduled status to be updated
// 	scheduledAt: timestamp of when the status should be scheduled
//
// Returns the updated scheduled status
func UpdateScheduledStatus(baseUrl, accessToken, scheduledId, scheduledAt string) (ScheduledStatus, error) {
	requestUrl := constructUrl(baseUrl, "/api/v1/scheduled_statuses/"+scheduledId)
	var stat ScheduledStatus
	_, _, err := apiCallJson(requestUrl, http.MethodPut, accessToken, map[string]interface{}{"scheduled_at": scheduledAt}, &stat)
	if err != nil {
		return ScheduledStatus{}, err
	}
	return stat, nil
}

// Deletes a scheduled status
//
//	scheduledId: identifier of the scheduled status to be updated
func DeleteScheduledStatus(baseUrl, accessToken, scheduledId string) error {
	requestUrl := constructUrl(baseUrl, "/api/v1/scheduled_statuses/"+scheduledId)
	var stat ScheduledStatus
	_, _, err := apiCall(requestUrl, http.MethodDelete, accessToken, nil, &stat)
	if err != nil {
		return err
	}
	return nil
}
