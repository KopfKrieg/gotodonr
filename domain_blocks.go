package gotodonr

import (
	"net/http"
)

// Get a page of domains the user has blocked
//
//	limit: max amount of results per page
//
// Returns a page of domains the user has blocked and urls pointing to the previous and next page respectively
func GetDomainBlocks(baseUrl, accessToken string, limit int) ([]string, string, string, error) {
	data := map[string]interface{}{}
	if limit > 0 {
		data["limit"] = getLimit(limit)
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/domain_blocks", data)
	var str []string
	headers, _, err := apiCall(requestUrl, http.MethodGet, accessToken, nil, &str)
	if err != nil {
		return nil, "", "", err
	}
	prev, next := readLinkHeader(headers)
	return str, next, prev, nil
}

// Gets a book which can be used to scroll through pages of domains the user has blocked
//
//	limit: max amount of results per page
//
// Returns a StringBook which can be used to scroll through the pages or collect all pages at once
func GetDomainBlocksBook(baseUrl, accessToken string, limit int) StringBook {
	data := map[string]interface{}{}
	if limit > 0 {
		data["limit"] = getLimit(limit)
	}
	requestUrl := constructGetUrl(baseUrl, "/api/v1/domain_blocks", data)
	var book = NewStringBook(accessToken)
	book.Update(requestUrl)
	return book
}

// Blocks the given domain (strip any protocols and such, domain needs to be 'domain.tld')
//
//	domain: the domain to be blocked
func BlockDomain(baseUrl, accessToken, domain string) error {
	requestUrl := constructUrl(baseUrl, "/api/v1/domain_blocks")
	_, _, err := apiCallJson(requestUrl, http.MethodPost, accessToken, map[string]interface{}{"domain": domain}, nil)
	if err != nil {
		return err
	}
	return nil
}

// Unblocks the given domain (strip any protocols and such, domain needs to be 'domain.tld')
//
//	domain: the domain to be unblocked
func UnblockDomain(baseUrl, accessToken, domain string) error {
	requestUrl := constructUrl(baseUrl, "/api/v1/domain_blocks")
	_, _, err := apiCallJson(requestUrl, http.MethodDelete, accessToken, map[string]interface{}{"domain": domain}, nil)
	if err != nil {
		return err
	}
	return nil
}
